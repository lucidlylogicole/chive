# Chive Notebook
Chive Notebook is an *ar*chive for notetaking, files, links, and more.

Notes are written and stored in [Markdown](https://commonmark.org/). 

<a href="docs/chive_page.png" target="_blank" title="Chive Notebook View"><img src="docs/chive_page.png" width=440></a>
<a href="docs/chive_editor.png" target="_blank" title="Chive Editor"><img src="docs/chive_editor.png" width=440></a>

## Features
- Organize notes, attachments, and links into multiple notebook folders
- Access Chive through a web browser or the built in interface (PyQt5 or Electron required)
- Markdown editor for notes
- Notes are stored in a SQLite database
- Easily find your notes, files, and links by searching by text, date, notebook, and recent
- Tasks section to keep track of what you need to do
- Your notes are stored locally by default, but since Chive runs off of the Bottlepy web server, you could run your notebook on your personal network.
    - *Since Chive saves and opens files, do not run this off a public network at this time*

## Requirements
- Python 3.6+
- Modern Web Browser
    - or (Optional) PyQt5.10+ and QtWebEngineWidgets - to utilize Chive's built in browser
    - or (Optional) [Electronjs](https://electronjs.org/) - use as a web browser to wrap Chive as an app
- (Optional) [gevent](http://www.gevent.org/) - Install in Python for an asyncronous web server and to optionally enable SSL

## Install 
1. [Download](https://gitlab.com/lucidlylogicole/chive/-/archive/master/chive-master.zip) this repo
2. Set up one of the run options in the next step.

## Run Chive
There are 3 options to run Chive so you can utilize whatever is easiest for your system.

### 1. Run - Using your own Web Browser
2. Run `python3 chive.py`
3. Point your browser to `http://localhost:9876` or the port specified in the settings
    - Don't forget to turn on JavaScript!

### 2. Run - Using a PyQt Web Browser
2. Run `python3 chive_browser.py`
    - PyQt5.10+ and QtWebEngineWidgets must be installed

### 3. Run using Electron as a Web Browser
2. Download the [Electron release](https://electronjs.org/releases/stable) for your OS like `electron-13.0.0-win32-x64.zip` for Windows
3. unzip the files to `chive/electron/bin`
4. Run 

        chive/electron/start_chive.sh

    or

        cd chive/electron
        bin/electron .

*None: You can utlize an another installation of Electron as no other dependencies are needed.  Just point Electron to chive/electron/main.js*

## Settings
To customize your Chive settings and specify the notebook path or Python path, edit the `.chive` file located in the user home directory.  If you start chive, this file will already be created from the default settings. Otherwise you can create your own before startup.

**~/.chive**

    # Path to Python executable
    pythonPath=python3

    # Path to where your notebook is stored
    path=~/chive_notebook

    # Port for localhost
    port=9876

    # Seconds to autosave when editing a note (set to 0 to turn off autosave)
    autoSave=3

    # Enable loading JavaScript plugins from your notebook folder
    pluginsEnabled=false

    # Enable SSL (To enable, specify the paths to the key and cert files)
    sslEnabled=false
    sslKeyFile=~/chive_notebook/.ssl/chive.key
    sslCertFile=~/chive_notebook/.ssl/chive.csr

### SSL
If enabling SSL you need to generate your own key and certificate and then specify the location to them.  A simple location is to just place them in a `.ssl` folder in the notebook folder.

    mkdir ~/chive_notebook/.ssl/
    cd ~/chive_notebook/.ssl/
    openssl req -x509 -newkey rsa:4096 -keyout chive.key -out chive.csr -days 365 -nodes -subj '/CN=localhost'

**Note: gevent must be installed with Python in order for SSL to work**

## Reference
Thanks to the following tools that Chive is built on. (Other than Python, these are not dependencies as Chive already includes them)

- [Python](http://python.org)
- [Bottlepy](https://bottlepy.org) - localhost web server
- [Ace Editor](http://ace.c9.io/) - HTML5 based code editor
- [cmarkd.js](https://gitlab.com/lucidlylogicole/cmarkd) - a modified commonmark parser utilized for [markdown](http://commonmark.org/)
- [PyQt](https://www.riverbankcomputing.com/software/pyqt/intro) - optional if utilizing the built in interface
- [Electronjs](https://electronjs.org/) - optional wrapper for the app

## License
- **[License](LICENSE)** - GNU General Public License (GPL 3)