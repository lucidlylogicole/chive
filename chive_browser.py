import sys, os, socket
from PyQt5 import QtCore, QtWidgets, QtGui, QtWebEngineWidgets

os.chdir(os.path.abspath(os.path.dirname(__file__)))
import chive

class ApplicationThread(QtCore.QThread):
    def __init__(self,port):
        QtCore.QThread.__init__(self)
        self.port = port
        
    def __del__(self):
        self.wait()

    def run(self):
        chive.start_chive(launch=0,use_port=self.port)


def errorHandler(*exc_info):
    import traceback
    print("".join(traceback.format_exception(*exc_info)), file=sys.stderr)

class ChiveBrowser:
    def __init__(self):
        self.port = chive.getPort(chive.getCurrentPort())
        
    def load(self):
        self.webview = QtWebEngineWidgets.QWebEngineView()
        self.webview.setWindowTitle('Chive')
        self.webview.setWindowIcon(QtGui.QIcon("style/chive.svg"))

        # Clear the page cache so it loads the latest 
        self.webview.page().profile().clearHttpCache()

        # Load url
        self.url = chive.chive_url

    def start_thread(self):
        self.AppThread = ApplicationThread(self.port)
        self.AppThread.start()

        import time
        time.sleep(1) # Make sure bottle starts

        return self.AppThread

    def stop_thread(self):
        self.AppThread.terminate()
    
    def seturlmode(self):
        self.webview.page().runJavaScript("CURRENT.url_mode='out'")

    def loadPage(self):
        self.webview.page().loadFinished.connect(self.seturlmode)
        self.webview.load(QtCore.QUrl(self.url+'/tabs'))

    def start(self):
        self.start_thread()
        self.load()
        self.loadPage()

#---Main
def start_browser():
    sys.excepthook = errorHandler
    
    # Application Level
    qtapp = QtWidgets.QApplication(sys.argv)

    browser = ChiveBrowser()
    browser.start()
    qtapp.aboutToQuit.connect(browser.stop_thread)
    browser.webview.resize(1400, 800)
    browser.webview.show()
    return qtapp.exec_()

start_browser()