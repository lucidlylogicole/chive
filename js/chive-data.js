
function data_post(url,data,error_msg) {
    var prom = $P.promise()
    $P.ajax({
        url:url,
        method: 'POST',
        responseType: 'json',
        contentType: 'application/json;charset=UTF-8',
        data:JSON.stringify(data),
        headers:{'chive':'allium'},
        success: function(result) {
            prom.accept(result)
        },
        error: function(status) {
            showError(error_msg)
            prom.cancel()
        }
    })
    return prom
}

function data_get(url,error_msg) {
    var prom = $P.promise()
    $P.ajax({
        url:url,
        method: 'GET',
        responseType: 'json',
        contentType: 'application/json;charset=UTF-8',
        headers:{'chive':'allium'},
        success: function(result) {
            prom.accept(result)
        },
        error: function(status) {
            showError(error_msg)
            prom.cancel()
        }
    })
    return prom
}

function queryData(cmd,params,write) {
    data = {
        cmd:cmd,
        write:0,
    }
    if (write !== undefined ){data.write=write}
    if (params !== undefined) {
        data.params=params
    }
    return data_post('query',data,'querying database')
}

function openUrlData(url) {
    data = {
        url:url,
    }
    return data_post('webbrowser',data,'querying database')
}

//---
//---Notebook
function getNotebooksData(options) {
    return data_post('notebooks',options,'Loading notebooks')
}

function newNotebookData(data) {
    return data_post('newnotebook',data,'Creating New Notebooks')
}

function renameNotebookData(data) {
    return data_post('renamenotebook',data,'Renaming Notebook')
}

function archiveNotebookData(data) {
    return data_post('archivenotebook',data,'Archiving Notebook')
}

function deleteNotebookData(data) {
    return data_post('deletenotebook',data,'Deleting Notebook')
}

//---
//---Content
function getContentList(options) {
    return data_post('getcontentlist',options,'Getting Content')
}

function getContentData(cid,qry) {
    if (qry === undefined) {qry=''}
    return data_get('getcontent/'+cid+qry,'Loading content data')
}

function saveContentData(contentD) {
    return data_post('savecontent',contentD,'Saving content')
}

function newContentData(contentD) {
    return data_post('newcontent',contentD,'Creating new content')
}

function deleteContentData(cid) {
    return data_post('deletecontent',{uuid:cid},'Deleting content')
}

function moveContentData(contentD) {
    return data_post('movecontent',contentD,'Moving content')
}

function archiveContentData(contentD) {
    return data_post('archivecontent',contentD,'Archiving content')
}

//---
//---Tasks
function getTaskData(options) {
    return data_post('gettasks',options,'Loading task data')
}

function saveTaskData(contentD) {
    return data_post('savetask',contentD,'Saving task data')
}
function deleteTaskData(tid) {
    return data_post('deletetask',{uuid:tid},'Deleting task')
}
function updateTaskData(options) {
    return data_post('updatetask',options,'Deleting task')
}
function getNotebookTaskData(options) {
    return data_post('getnotebooktasks',options,'Loading notebook task data')
}


//---
//---Sections
function getSectionsData(options) {
    return data_post('sections',options,'Loading sections data')
}

function newSectionData(options) {
    return data_post('newsection',options,'Creating new section')
}

function renameSectionData(options) {
    return data_post('renamesection',options,'Renaming section')
}

function deleteSectionData(sid) {
    return data_post('deletesection',{uuid:sid},'Deleting section')
}

//---
//---Files
function uploadFileData(data) {
    var prom = $P.promise()
    $P.ajax({
        url:'uploadfile',
        method: 'POST',
        responseType: 'json',
        data:data,
        headers:{'chive':'allium'},
        success: function(result) {
            prom.accept(result)
        },
        error: function(status) {
            showError('Uploading file')
            prom.cancel()
        }
    })
    return prom
}

function openFileData(data) {
    return data_post('openfile',data,'Opening File')
}

//---
//---Settings
function viewSettingsData() {
    return data_get('about')
}
