# --------------------------------------------------------------------------------
# This module is licensed under the GNU General Public License (GPL 3)
# --------------------------------------------------------------------------------

class DB(object):
    def close(self):
        '''Close Connection'''
        self.cur.close()
        self.con.close()

    def cmd(self, sqlCommand, rORw=0, params=None, keepOpen=0, **kargs):
        # Initial Variables
        result = []
        
        # Connect to databse
        self.open()
        
        if params == None:
            self.cur.execute(sqlCommand)
        else:
            self.cur.execute(sqlCommand,params)
        
        if rORw == 1: # Write
            self.con.commit()
        else: # Read
            columns = self.getCursorDescription()
            
            # Get all results
            result = self.cur.fetchall()
            res = {'cols':columns,'data':result}
            if 'returnDict' in kargs and kargs['returnDict']:
                return self.resultToDict(res)
            return res
        
        if not keepOpen:
            self.close() # Close Connection
    
    def resultToDict(self,result,nullToBlank=0):
        '''Convert the result to a rows of dictionaries with column names as the keys'''
        res = []
        for row in result['data']:
            rowD = {}
            for c in range(len(result['cols'])):
                v = row[c]
                if nullToBlank:
                    v = v if v!= None else ''
                rowD[result['cols'][c]]=v
            res.append(rowD)
        
        return res
    
    def getCursorDescription(self):
        cur_desc = self.cur.description
        columns = []
        for col in cur_desc:
            columns.append(col.name)
    
class SqliteDB(DB):
    def __init__(self,path="",**kargs):
        self.path = path
        
    def open(self):
        import sqlite3 as sqlite
        self.con = sqlite.connect(self.path)
        self.cur = self.con.cursor()  # Get a cursor object

    def getCursorDescription(self):
        cur_desc = self.cur.description
        columns = []
        for col in cur_desc:
            columns.append(col[0])
        
        return columns

class PostgresDB(DB):
    def __init__(self,*args,**kargs):
        '''Use psycopg2 connect arguments like:
                open(host='546.123.9.564', user='postgres', password='falsey2', dbname='test')
            or
                open('host=546.123.9.564 user=postgres password=falsey2 dbname=test')
        '''
        self.args = args
        self.kargs = kargs

    def open(self):
        import psycopg2
        self.con = psycopg2.connect(*self.args,**self.kargs)
        self.cur = self.con.cursor()
