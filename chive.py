import sys
import subprocess, socket
sys.path.append('site_pkg')
import bottle
from bottle import route, run, template, response, request, redirect, static_file
from db_commander import SqliteDB
import os, json, uuid, datetime, shutil, sys
bottle.BaseRequest.MEMFILE_MAX = 1024 * 1024 *10

# Try to use gevent
SERVER = 'wsgiref'
try:
    from gevent import monkey; monkey.patch_all()
    SERVER = 'gevent'
except:
    pass

note_path = ''
settings_path = os.path.join(os.path.expanduser('~'),'.chive')
__file_path = os.path.dirname(os.path.abspath(__file__))
DB = None

# Default Settings
settingD={'autoSave':3, 'pluginsEnabled':0, 'port':9876, 'pythonPath':'python'}
defaultSettingD={'autoSave':3, 'pluginsEnabled':0, 'port':9876, 'pythonPath':'python'}
settings_loaded = 0

# Get Version
with open('electron/package.json','r') as f:
    version = json.load(f)['version']

# Header Check
def headerCheck(fn):
    def _header_check(*args, **kwargs):
        if 'chive' in request.headers and request.headers['chive']=='allium':
            return fn(*args, **kwargs)
        else:
            return ''
    return _header_check

#---Main Pages
@route('/')
def index():
    vtest=1
    if not settings_loaded:
        return 'Settings have not been loaded.  Please edit the file:<br>'+settings_path
    else:
        with open('chive.html') as f:
            html =  f.read()
        
        # Load Plugins -------
        plugin_html = ''
        if settingD['pluginsEnabled'] == 'true':
            plugin_path = os.path.join(os.path.expanduser(settingD['path']),'chive_plugins')
            if os.path.exists(plugin_path):
                for fl in os.listdir(plugin_path):
                    if not fl.startswith('_'):
                        if plugin_html !='': plugin_html += '\n'
                        if fl.endswith('.js'):
                            plugin_html += '<script src="/files/chive_plugins/'+fl+'"></script>'
                        elif fl.endswith('.css'):
                            plugin_html += '<link href="/files/chive_plugins/'+fl+'" rel="stylesheet" media="screen">'
        #- ----------------------
        
        return html.replace('{{chive_plugins}}',plugin_html)

@route('/tabs')
def tabs():
    if not settings_loaded:
        return redirect('settings')
    else:
        return template('chive_tabs.html',chive_url=chive_url)

@route('/editor')
def editor():
    return template('editor.html',autoSave=settingD['autoSave'])

@route('/about')
@headerCheck
def about():
    return json.dumps({
        'port':settingD['port'],
        'notebook':settingD['path'],
        'autoSave':settingD['autoSave'],
        'version':version,
    })

#---
#---Settings
def loadSettings():
    global DB,note_path,settingD
    
    # Load Default Settings
    with open('default_settings','r') as f:
        settingD = parseSettings(f.read())
    if not os.path.exists(settings_path):
        # Copy default settings
        import shutil
        shutil.copyfile('default_settings',settings_path)
    if os.path.exists(settings_path):
        # Load Settings File
        with open(settings_path,'r') as f:
            sD = parseSettings(f.read())
        for ky in sD:
            settingD[ky] = sD[ky]

        if 'path' in settingD and os.path.exists(os.path.join(os.path.expanduser(settingD['path']),'notebook.db')):
            # Load Notebook
            note_path = os.path.expanduser(settingD['path'])
            DB = SqliteDB(os.path.join(note_path,'notebook.db'))            
            return 1
        else:
            spath = os.path.expanduser(settingD['path'])
            if not os.path.exists(spath):
                try:
                    os.mkdir(spath)
                except:
                    print('error creating notebook directory')
            if os.path.exists(spath):
                note_path = spath
                
                # Setup notebook
                DB = SqliteDB(os.path.join(note_path,'notebook.db'))
                return setupNotebook()
            
    return 1

def parseSettings(txt):
    sD ={}
    for rw in txt.splitlines():
        ln = rw.strip()
        if not ln.startswith('#') and '=' in ln:
            ky,val = ln.split('=')
            sD[ky.strip()] = val.strip()
    return sD
    
def setupNotebook():
    # Content
    DB.cmd("""CREATE TABLE content ( 
        uuid        VARCHAR  PRIMARY KEY,
        typ         VARCHAR,
        title       VARCHAR,
        nbk_uuid    VARCHAR,
        create_time DATETIME,
        modify_time DATETIME,
        content     VARCHAR,
        pin         BOOLEAN     DEFAULT ( 0 ),
        fave        BOOLEAN     DEFAULT ( 0 ),
        archive     BOOLEAN     DEFAULT ( 0 ),
        parent      VARCHAR,
        section     VARCHAR,
        tags        VARCHAR
        )""",1)
    
    # Notebooks
    DB.cmd("""
        CREATE TABLE notebooks ( 
        uuid        VARCHAR     PRIMARY KEY,
        name        VARCHAR,
        parent      VARCHAR,
        typ         VARCHAR     DEFAULT ( 'notebook' ),
        archive     BOOLEAN     DEFAULT ( 0 )
        )""",1)
    
    # Tasks
    DB.cmd("""
        CREATE TABLE tasks ( 
        uuid          VARCHAR  PRIMARY KEY,
        title         VARCHAR,
        status        VARCHAR,
        nbk_uuid      VARCHAR,
        priority      VARCHAR,
        section       VARCHAR,
        create_time   DATETIME,
        complete_time DATETIME,
        modify_time   DATETIME,
        target_date   DATE,
        plan_date     DATE,
        content       VARCHAR,
        content_uuid  VARCHAR,
        tags          VARCHAR
        )""",1)
    
    # Setup Plugin Directory
    os.mkdir(os.path.join(note_path,'chive_plugins'))
    
    return 1

#---
#---Static / Files
@route('/style/<filename:path>')
def static_style(filename):
    return static_file(filename, root=os.path.join(__file_path,'style'))

@route('/js/<filename:path>')
def static_js(filename):
    return static_file(filename, root=os.path.join(__file_path,'js'))

@route('/files/<filename:path>')
def notebookFiles(filename):
    return static_file(filename, root=os.path.abspath(note_path))

@route('/file/<uid>')
def notebookFile(uid):
    db_cmd = "SELECT C.title,N.name,C.content FROM content C LEFT JOIN notebooks N ON C.nbk_uuid = N.uuid WHERE C.uuid=?"
    res = DB.cmd(db_cmd,0,params=[uid])
    
    if res['data'] != []:
        nbk = res['data'][0][1]
        if nbk in ['','None',None]: nbk = 'misc'
        path = os.path.join(nbk,res['data'][0][0])
        return static_file(path, root=os.path.abspath(note_path))

@route('/uploadfile',method='POST')
@headerCheck
def uploadFile():
    data = request.files.filedata
    nbk_uuid = request.forms.nbk_uuid
    filename = request.forms.filename
    parent = request.forms.parent
    typ = request.forms.typ
    if typ in ['',None]: typ = 'file'
    if parent == '': parent = None
    ext = request.forms.ext
    uid = None
    if filename in [None,'','undefined','null']: 
        filename = str(uuid.uuid4().hex)
        uid = filename
        if not ext in [None,'']:
            filename += '.'+ext
    
    if nbk_uuid in [None,'null','undefined']: nbk_uuid = ''
    
    db_cmd = "SELECT name FROM notebooks WHERE uuid=?"
    res = DB.cmd(db_cmd,0,params=[nbk_uuid])
    if res['data'] != []:
        nbk_name = res['data'][0][0]
    
    else:
        nbk_name = 'misc'
        
    if not os.path.exists(os.path.join(note_path,nbk_name)):
        os.mkdir(os.path.join(note_path,nbk_name))

    dst = os.path.join(note_path,nbk_name,filename)
    
    if os.path.exists(dst):
        return {'status':'file already exists'}
    
    if data != '':
        with open(dst,'wb') as open_file:
            open_file.write(data.file.read())
    else:
        open(dst, 'a').close()
    
    newD = {
        'title':filename,
        'typ':typ,
        'nbk_uuid':nbk_uuid,
        'content':'',
        'parent':parent
    }
    if uid != None: newD['uuid']=uid
    
    # Upload to database
    ncD = new_content(newD)
    ncD['folder'] = nbk_name
    ncD['filename'] = filename
    return ncD

@route('/openfile',method='POST')
@headerCheck
def openFile():
    data = request.json
    
    db_cmd = "SELECT C.title,N.name,C.content FROM content C LEFT JOIN notebooks N ON C.nbk_uuid = N.uuid WHERE C.uuid=?"
    res = DB.cmd(db_cmd,0,params=[request.json['uuid']])
    
    if res['data'] != []:
        nbk = res['data'][0][1]
        if nbk in ['','None',None]: nbk = 'misc'
        path = os.path.join(note_path,nbk,res['data'][0][0])
        
        if not os.path.exists(path):
            raise Exception('File Not Found: '+path)
        else:
            if os.name == 'nt':
                os.startfile(path)
            elif os.name=='posix':
                subprocess.Popen(['xdg-open', path],cwd=os.path.dirname(path))
            elif os.name=='mac':
                subprocess.Popen(['open', path],cwd=dpth)

@route('/opennotebookfolder',method='POST')
@headerCheck
def openNotebookFolder():
    db_cmd = "SELECT name FROM notebooks WHERE uuid=?"
    res = DB.cmd(db_cmd,0,params=[request.json['uuid']])
    if res['data'] != []:
        nbk = res['data'][0][0]
        if nbk in ['','None',None]: nbk = 'misc'
        path = os.path.join(note_path,nbk)
        
        if os.path.exists(path):
            if os.name == 'nt':
                os.startfile(path)
            elif os.name=='posix':
                subprocess.Popen(['xdg-open', path])
            elif os.name=='mac':
                subprocess.Popen(['open', path])
            
            return {'status':'ok'}
                
    return {'status':'folder not found'}
    
#---
#---Notebook
@route('/notebooks',method='POST')
@headerCheck
def notebooks():
    data = request.json
    db_wh = ""
    
    res = DB.cmd("SELECT uuid,name,parent,typ,archive FROM notebooks "+db_wh+" ORDER BY typ,LOWER(name)",returnDict=1)
    res2 = DB.cmd("""SELECT DISTINCT strftime('%Y', create_time) FROM content
        ORDER BY strftime('%Y', create_time) DESC""")
    yrs = []
    yr = str(datetime.datetime.today().year)
    for r in res2['data']:
        yrs.append(r[0])
    if not yr in yrs:
        yrs.insert(0,yr)
    
    # Setup notebooks
    nbks = {}
    secs = {}
    for rD in res:
        if rD['typ'] == 'notebook':
            nbks[rD['uuid']] = {'name':rD['name'],'parent':rD['parent'],'archive':rD['archive'],'sections':[]}
        # Setup Sections
        elif rD['typ'] == 'section':
            secs[rD['uuid']] = {'name':rD['name'],'nbk_uuid':rD['parent'],'archive':rD['archive']}
            nbks[rD['parent']]['sections'].append(rD['uuid'])
    
    return json.dumps({'notebooks':nbks,'sections':secs,'years':yrs,'path':os.path.abspath(note_path)})


@route('/newnotebook',method='POST')
@headerCheck
def newNotebook():
    
    data = request.json
    uid = str(uuid.uuid4().hex)

    db_cmd = "INSERT INTO notebooks (uuid,name) VALUES (?,?)"
    DB.cmd(db_cmd,1,params=[uid,data['name']])
    
    return {'uuid':uid}

@route('/renamenotebook',method='POST')
@headerCheck
def renameNotebook():
    data = request.json
    txt = 'ok'

    ok = 1
    # Rename Folder
    db_cmd = "SELECT name FROM notebooks WHERE uuid=?"
    res = DB.cmd(db_cmd,0,params=[data['uuid']])
    if res['data'] != []:
        old_pth = os.path.join(note_path,res['data'][0][0])
        new_pth = os.path.join(note_path,data['name'])
        if os.path.exists(old_pth):
            try:
                os.rename(old_pth,new_pth)
                ok = 1
            except:
                ok = 0
                txt = 'could not rename the folder (maybe a file is open in the folder)'
        
    if ok:
        db_cmd = "UPDATE notebooks set name=? WHERE uuid=?"
        DB.cmd(db_cmd,1,params=[data['name'],data['uuid']])
        
        txt = 'ok'
        
    return json.dumps({'response':txt})

@route('/deletenotebook',method='POST')
@headerCheck
def deleteNotebook():
    data = request.json

    # Delete Content
    db_cmd = "DELETE FROM content WHERE nbk_uuid=?"
    DB.cmd(db_cmd,1,params=[data['uuid']])
    
    # Delete Tasks
    db_cmd = "DELETE FROM tasks WHERE nbk_uuid=?"
    DB.cmd(db_cmd,1,params=[data['uuid']])

    # Delete Sections
    db_cmd = "DELETE FROM notebooks WHERE parent=?"
    DB.cmd(db_cmd,1,params=[data['uuid']])

    # Delete Notebook
    db_cmd = "DELETE FROM notebooks WHERE uuid=?"
    DB.cmd(db_cmd,1,params=[data['uuid']])
    
    return json.dumps({'response':'ok'})

@route('/archivenotebook',method='POST')
@headerCheck
def archivenotebook():
    cD = request.json
    uid = str(cD['uuid'])
    arc = cD['archive']
    ok = 'ok'
    try:
        db_cmd = "UPDATE notebooks set archive=? WHERE uuid=?"
        DB.cmd(db_cmd,1,params=[arc,uid])
    except:
        ok = 'could not archive the content'
    return json.dumps({'response':ok})

#---
#---Sections
@route('/sections',method='POST')
@headerCheck
def sections():
    data = request.json
    res = DB.cmd("SELECT uuid,name,parent FROM notebooks WHERE typ='section' AND parent=? ORDER BY LOWER(name)",returnDict=1,params=[data['nbk_uuid']])
    return json.dumps({'sections':res})


@route('/newsection',method='POST')
@headerCheck
def newSection():
    data = request.json
    uid = str(uuid.uuid4().hex)

    db_cmd = "INSERT INTO notebooks (uuid,name,typ,parent) VALUES (?,?,'section',?)"
    DB.cmd(db_cmd,1,params=[uid,data['name'],data['nbk_uuid']])
    
    return {'uuid':uid}

@route('/renamesection',method='POST')
@headerCheck
def renameSection():
    data = request.json

    db_cmd = "UPDATE notebooks set name=? WHERE uuid=?"
    DB.cmd(db_cmd,1,params=[data['name'],data['uuid']])
    
    txt = 'ok'
    
    return json.dumps({'response':txt})

@route('/deletesection',method='POST')
@headerCheck
def deleteSection():
    data = request.json

    # Delete Content
    db_cmd = "UPDATE content SET section='' WHERE section=?"
    DB.cmd(db_cmd,1,params=[data['uuid']])
    
    # Delete Tasks
    db_cmd = "UPDATE tasks SET section='' WHERE section=?"
    DB.cmd(db_cmd,1,params=[data['uuid']])
    
    # Delete Notebook
    db_cmd = "DELETE FROM notebooks WHERE uuid=?"
    DB.cmd(db_cmd,1,params=[data['uuid']])
    
    return json.dumps({'response':'ok'})

#---
#--- Content
@route('/getcontentlist',method='POST')
@headerCheck
def getContentList():
    db_wh = ""
    cols = ''
    db_order = 'C.pin DESC,C.create_time DESC'
    db_join = ''
    params = []
    
    options = request.json
    # Pinned Articles
    if 'pin' in options and options['pin']:
        if db_wh != '': db_wh += ' AND'
        db_wh += " C.pin=1"
    
    # Types
    if 'typ' in options:
        typs = options['typ']
    else:
        typs = ['file','note','link']
    if typs != 'all':
        if db_wh != '': db_wh += ' AND '
        db_wh += " C.typ IN ("
        for tp in typs:
            if db_wh != " C.typ IN (": db_wh += ','
            db_wh += '?'
            params.append(tp)
        db_wh += ')'
    
    if 'parent' in options:
        if db_wh != '': db_wh += ' AND '
        if options['parent'] == None:
            db_wh += " C.parent IS NULL"
        else:
            db_wh += "C.parent=?"
            params.append(options['parent'])
    # Favorites
    
    # Notebook
    if 'nbk_id' in options:
        cols += ',section'
        db_order = 'C.pin DESC,C.create_time DESC'
        db_join = " LEFT OUTER JOIN notebooks S ON C.section = S.uuid"
        if db_wh != '': db_wh += ' AND'
        if options['nbk_id'] == 'None':
            db_wh += " C.nbk_uuid IS NULL"
        else:
            db_wh += " C.nbk_uuid=?"
            params.append(options['nbk_id'])
    
    # Section
    if 'section' in options:
        if db_wh != '': db_wh += ' AND'
        db_wh += " C.section=?"
        params.append(options['section'])
    
    # Archived
    if not options['archive']:
        if db_wh != '': db_wh += ' AND'
        db_wh += " NOT C.archive"

    # Recent
    if 'recent' in options:
        dys = int(options['recent'])
        dt = (datetime.datetime.today()-datetime.timedelta(days=dys)).strftime('%Y-%m-%d')
        if db_wh != '': db_wh += ' AND'
        db_wh += " (C.create_time >= date(?) OR (C.modify_time >= date(?) AND C.pin=1) )"
        params.append(dt)
        params.append(dt)
        
    # Month
    if 'month' in options and 'year' in options:
        dt = datetime.datetime(int(options['year']),int(options['month']),1).strftime('%Y-%m')
        if db_wh != '': db_wh += ' AND'
        db_wh += " strftime('%Y-%m', C.create_time) = ?"
        params.append(dt)
    
    # Search
    if 'search_text' in options:
        delim = ','
        db_join = " LEFT OUTER JOIN notebooks N ON C.nbk_uuid = N.uuid"
        if '&&' in options['search_text']: delim = '&&'
        if '||' in options['search_text']: delim = '||'
        stxts = options['search_text'].lower().split(delim)
        s_wh = ''
        for stxt in stxts:
            if s_wh != '': s_wh += {'&&':' AND',',':' OR','||':' OR'}[delim]
            s_wh += " (LOWER(C.title) LIKE ?"
            if options['content']:
                s_wh += " OR LOWER(C.content) LIKE ?"
                params.append('%'+stxt.strip()+'%')
            s_wh += ")"
            params.append('%'+stxt.strip()+'%')
        if db_wh != '': db_wh += ' AND'
        db_wh += ' ( '+s_wh+')'

        if not options['archive']:
            db_wh += " AND (NOT N.archive OR N.archive IS NULL)"
    
    if 'get_content' in options and options['get_content']:
        cols += ',content'
    
    db_cmd = """SELECT C.uuid,C.nbk_uuid,C.typ,C.title,C.create_time,C.parent,C.pin,C.archive{1} 
        FROM content C {3} WHERE {0}
        ORDER BY {2}""".format(db_wh,cols,db_order,db_join)

    res = DB.cmd(db_cmd,params=params,returnDict=1)
    return json.dumps(res)

@route('/getcontent/<cid>')
@headerCheck
def getcontent(cid):
    db_cmd = """SELECT typ,title,content,create_time,modify_time,parent,nbk_uuid,pin,section,archive 
        FROM content WHERE uuid=?"""
    res = DB.cmd(db_cmd,params=[cid],returnDict=1)
    if len(res) > 0:
        contentD = res[0]
        if contentD['typ'] == 'note':
            if contentD['content'] == None: contentD['content'] = ''
        return json.dumps(contentD)
    else:
        return {}

@route('/savecontent',method='POST')
@headerCheck
def saveContent():
    cD = request.json
    
    db_cmd = "SELECT C.title,N.name,C.nbk_uuid FROM content C LEFT JOIN notebooks N ON C.nbk_uuid = N.uuid WHERE C.uuid=?"
    res = DB.cmd(db_cmd,0,params=[cD['uuid']])
    
    ok = 'ok'
    
    # Check if notebook changed
    if cD['nbk_uuid'] != res['data'][0][2]:
        ok = move_content(cD['uuid'],cD['nbk_uuid'],cD['section'])
    
    # Check for file rename
    if cD['typ'] == 'file':
        # db_cmd = "SELECT C.title,N.name,C.nbk_uuid FROM content C LEFT JOIN notebooks N ON C.nbk_uuid = N.uuid WHERE C.uuid=?"
        # res = DB.cmd(db_cmd,0,params=[cD['uuid']])
        rename_title = ''
        if res['data'] != []:
            if res['data'][0][0] != cD['title']:
                rename_title = res['data'][0][0]
                nbk_name = res['data'][0][1]
        
        if rename_title != '':
            old_pth = os.path.join(note_path,nbk_name,rename_title)
            new_pth = os.path.join(note_path,nbk_name,cD['title'])
            if os.path.exists(old_pth):
                try:
                    os.rename(old_pth,new_pth)
                except:
                    ok = 'Could not rename file'
    
    # Update Content
    if ok == 'ok':
        db_cmd = "UPDATE content SET title=?,content=?,modify_time=datetime(current_timestamp, 'localtime'),nbk_uuid=?,section=? WHERE uuid=?"
        DB.cmd(db_cmd,1,params=[cD['title'],cD['content'],cD['nbk_uuid'],cD['section'],cD['uuid']])
    
    return json.dumps({'response':ok})

@route('/newcontent',method='POST')
@headerCheck
def newContent():
    return new_content(request.json)
    
def new_content(cD):
    cols = 'uuid,'
    if 'uuid' in cD and cD['uuid'] != 'new':
        uid = cD['uuid']
    else:
        uid = str(uuid.uuid4().hex)
    params = [uid]
    pcols = '?,'
    for ky in ['title','typ','nbk_uuid','content','pin','fave','parent','section']:
        if ky in cD:
            cols += ky+','
            params.append(cD[ky])
            pcols += '?,'
        
    db_cmd = """INSERT INTO content ({0}modify_time,create_time)
        VALUES ({1}datetime(current_timestamp, 'localtime'),datetime(current_timestamp, 'localtime'))""".format(cols,pcols)
    DB.cmd(db_cmd,1,params=params)
    
    return {'uuid':uid}

def delete_content(uid):
    ok = 'ok'

    # Delete Children First
    db_cmd = "SELECT uuid FROM content WHERE parent=?"
    res = DB.cmd(db_cmd,0,params=[uid])
    for r in res['data']:
        ok = delete_content(r[0])
    
    # Delete File if exists
    db_cmd = "SELECT C.typ,C.title,N.name FROM content C LEFT JOIN notebooks N ON C.nbk_uuid == N.uuid WHERE C.uuid=?"
    res = DB.cmd(db_cmd,0,params=[uid])
    if res['data'] == []:
        ok = 'content not found (%s)' %uuid
    else:
        typ = res['data'][0][0]
        if typ in ['file','attach']:
            nm = res['data'][0][1]
            nbk_nm = res['data'][0][2]
            if nbk_nm in ['None','',None]: nbk_nm = 'misc'
            filename = os.path.join(note_path,nbk_nm,nm)
            
            if os.path.exists(filename) and os.path.isfile(filename):
                try:
                    os.remove(filename)
                except:
                    ok = 'could not delete the file '+filename
    
    # Remove from 
    if ok == 'ok':
        db_cmd = "DELETE FROM content WHERE uuid=?"
        DB.cmd(db_cmd,1,params=[uid])
    
    return ok

@route('/deletecontent',method='POST')
@headerCheck
def deleteContent():
    cD = request.json
    uid = str(cD['uuid'])
    ok = delete_content(uid)
    return json.dumps({'response':ok})

def move_content(uuid,nbk_uuid,sec_uuid=None):
    ok = 'ok'
    
    # Get Current Content Info
    db_cmd = """SELECT C.nbk_uuid,C.typ,C.parent,N.name, C.title
        FROM content C
        LEFT JOIN notebooks N ON C.nbk_uuid = N.uuid
        WHERE C.uuid=?"""
    res = DB.cmd(db_cmd,0,params=[uuid])['data']
    rw = res[0]
    cur_folder = rw[3]
    
    if rw[0] != nbk_uuid:
        move_files = []
        
        # Move if different notebook
        if rw[1] == 'file':
            # Move File
            move_files.append([cur_folder,rw[4],None])
            
        elif rw[2] == None or rw[2] == '':
            # Move child content
            db_cmd = """SELECT C.nbk_uuid, N.name, C.title, C.uuid
                FROM content C
                LEFT JOIN notebooks N ON C.nbk_uuid = N.uuid
                WHERE C.parent=? AND C.typ = 'attach'"""
            res = DB.cmd(db_cmd,0,params=[uuid])['data']
            for r in res:
                if r[0] != nbk_uuid:
                    move_files.append([r[1],r[2],r[3]])

        if move_files != []:
            
            # Get new notebook Name
            db_cmd = "SELECT name FROM notebooks WHERE uuid=?"
            nres = DB.cmd(db_cmd,0,params=[nbk_uuid])['data']
            if nres == []:
                new_folder = 'misc'
            else:
                new_folder = nres[0][0]
            
            # Move Files
            for mf in move_files:
                old_folder = mf[0]
                if old_folder == '' or old_folder == None:
                    old_folder='misc'
                print(os.path.join(note_path,old_folder,mf[1]),os.path.join(note_path,new_folder,mf[1]))
                if not os.path.exists(os.path.join(note_path,new_folder)):
                    os.mkdir(os.path.join(note_path,new_folder))
                
                shutil.move(os.path.join(note_path,old_folder,mf[1]),os.path.join(note_path,new_folder,mf[1]))
                
                # Update database if not main content
                if mf[2] != None:
                    db_cmd = "UPDATE content SET nbk_uuid=?, section=? WHERE uuid=?"
                    DB.cmd(db_cmd,1,params=[nbk_uuid,sec_uuid,mf[2]])
    
    # Update Main Content nbk_uuid
    # try:
    db_cmd = "UPDATE content SET nbk_uuid=?, section=? WHERE uuid=?"
    DB.cmd(db_cmd,1,params=[nbk_uuid,sec_uuid,uuid])
    # except:
    #     ok = 'could not move the content'
    
    return ok

@route('/movecontent',method='POST')
@headerCheck
def movecontent():
    cD = request.json
    uid = str(cD['uuid'])
    nbk_uuid = str(cD['nbk_uuid'])
    sec_uuid = None
    if 'sec_uuid' in cD:
        sec_uuid = str(cD['sec_uuid'])
        
    
    return json.dumps({'response':move_content(uid,nbk_uuid,sec_uuid)})

    # return json.dumps({'response':ok})

@route('/archivecontent',method='POST')
@headerCheck
def archivecontent():
    cD = request.json
    uid = str(cD['uuid'])
    arc = cD['archive']
    
    ok = 'ok'
    try:
        db_cmd = "UPDATE content set archive=? WHERE uuid=?"
        DB.cmd(db_cmd,1,params=[arc,uid])
    except:
        ok = 'could not archive the content'
    
    return json.dumps({'response':ok})

#---
#---Tasks
@route('/gettasks',method='POST')
@headerCheck
def getTasks():
    aD = request.json
    
    # Initial Variables
    qry_params = []
    db_wh = ''
    db_order = ' create_time'
    cols = "uuid,title,status,priority,target_date,plan_date,section,content_uuid"
    
    if aD['mode'] == 'notebook':
        # All tasks for a notebook
        db_wh += " nbk_uuid=?"
        qry_params.append(aD['nbk_uuid'])
    elif aD['mode'] == 'task':
        # Single Task
        db_wh += " uuid=?"
        qry_params.append(aD['uuid'])
        aD['all_cols']=1
    elif aD['mode'] == 'search':
        # Search
        delim = ','
        if '&&' in aD['search']: delim = '&&'
        if '||' in aD['search']: delim = '||'
        stxts = aD['search'].lower().split(delim)
        s_wh = ''
        for stxt in stxts:
            if s_wh != '': s_wh += {'&&':' AND',',':' OR','||':' OR'}[delim]
            s_wh += " (LOWER(title) LIKE ? OR LOWER(content) LIKE ?)"
            qry_params.append('%'+stxt.strip()+'%')
            qry_params.append('%'+stxt.strip()+'%')
        if db_wh != '': db_wh += ' AND'
        db_wh = ' ( '+s_wh+')'
    elif aD['mode'] == 'month':
        # Calendar Month
        dt = aD['date']
        st_dt = datetime.date(aD['date'][0],aD['date'][1],1)
        end_dt = (st_dt + datetime.timedelta(days=32)).replace(day=1)
        dt_str = " ({0} >= date('%s') AND {0} < date('%s'))" %(st_dt.strftime('%Y-%m-%d'),end_dt.strftime('%Y-%m-%d'))
        dbc_wh = ""
        for dtcol in ['plan_date','complete_time','create_time']:
            if dbc_wh != "": dbc_wh += " OR"
            dbc_wh += dt_str.format(dtcol)
        db_wh = " (%s)" %dbc_wh
    elif aD['mode'] == 'recent':
        if aD['view'] == 'week':
            # Recent (today + 7 days)
            db_wh = " (NOT status in ('done','cancel') AND plan_date <= date(current_timestamp, 'localtime','+7 day') )"
        elif aD['view'] == 'today':
            db_wh = " (NOT status in ('done','cancel') AND plan_date <= date(current_timestamp, 'localtime') )"
        else:
            # Active
            db_wh = " status='active'"
        cols += ',nbk_uuid'
        
    if db_wh == '':
        return json.dumps({'tasks':[]})
    if not ('all_status' in aD and aD['all_status']) and aD['mode'] != 'task' and aD['mode'] != 'month':
        db_wh += " AND (NOT status in ('done','cancel'))"
    
    # Columns
    if 'all_cols' in aD and aD['all_cols']:
        cols += ",create_time,complete_time,modify_time,content,nbk_uuid"
    
    db_cmd = "SELECT {0} FROM tasks WHERE {1} ORDER BY {2}".format(cols,db_wh,db_order)
    res = DB.cmd(db_cmd,params=qry_params,returnDict=1)
    return json.dumps({'tasks':res})
    
@route('/savetask',method='POST')
@headerCheck
def saveTask():
    cD = request.json
    contD = {
        'notes':cD['task-edit-notes'],
    }

    params = [
        cD['task-edit-title'],
        cD['task-edit-status'],
        cD['task-edit-priority'],
        cD['task-edit-plan-date'],
        cD['task-edit-due-date'],
        json.dumps(contD),
        cD['nbk_uuid'],
        cD['sec_uuid'],
        cD['task-edit-note-link'],
    ]
    
    if cD['uuid'] == 'new':
        t_uuid = str(uuid.uuid4().hex)
        # New Task
        db_cmd = """INSERT INTO tasks ( 
                title,
                status,
                priority,
                plan_date,
                target_date,
                content,
                nbk_uuid,
                create_time,
                modify_time,
                section,
                content_uuid,
                uuid
            ) VALUES (
                ?,
                ?,
                ?,
                date(?),
                date(?),
                ?,
                ?,
                datetime(current_timestamp, 'localtime'),
                datetime(current_timestamp, 'localtime'),
                ?,
                ?,
                ?
            )"""
        
        DB.cmd(db_cmd,1,params=params+[t_uuid])
    else:
        t_uuid = cD['uuid']
        # Update Task
        db_cmd = """UPDATE tasks SET 
            title=?,
            status=?,
            priority=?,
            plan_date=date(?),
            target_date=date(?),
            content=?,
            nbk_uuid=?,
            section=?,
            content_uuid=?,
            modify_time=datetime(current_timestamp, 'localtime')
            WHERE uuid=?"""
        DB.cmd(db_cmd,1,params=params+[cD['uuid']])
    
    # Set complete date if done
    if cD['task-edit-status'] in ['done','cancel']:
        db_cmd = "UPDATE tasks SET complete_time = datetime(current_timestamp, 'localtime') WHERE uuid=?"
        DB.cmd(db_cmd,1,params=[t_uuid])
    
    return json.dumps({'response':'ok','uuid':t_uuid})

@route('/updatetask',method='POST')
@headerCheck
def updateTask():
    cD = request.json
    uid = str(cD['uuid'])
    # Only allow certain columns
    col = None
    if cD['col'] in ['status','priority','plan_date']:
        col = cD['col']
    
    if col != None:
        DB.cmd("UPDATE tasks SET "+col+"=? WHERE uuid=?",1,params=[cD['val'],uid])
        return json.dumps({'response':'ok'})

@route('/deletetask',method='POST')
@headerCheck
def deleteTask():
    cD = request.json
    uid = str(cD['uuid'])
    db_cmd = "DELETE FROM tasks WHERE uuid=?"
    DB.cmd(db_cmd,1,params=[uid])
    
    return json.dumps({'response':'ok'})

@route('/getnotebooktasks',method='POST')
@headerCheck
def getNotebookTasks():
    db_cmd = """SELECT N.uuid,N.name,
            SUM(CASE WHEN T.status='active' THEN 1 ELSE 0 END) AS cnt_active,
            SUM(CASE WHEN T.status='backlog' THEN 1 ELSE 0 END) AS cnt_backlog
        FROM tasks T
        LEFT JOIN notebooks N ON T.nbk_uuid = N.uuid
        WHERE T.status IN ('active','backlog')
        GROUP BY  N.uuid,N.name
        ORDER BY LOWER(N.name)
        """
    res = DB.cmd(db_cmd,returnDict=1)
    return json.dumps({'tasks':res})

#---
#---Tools
@route('/query',method='POST')
@headerCheck
def query():
    cD = request.json
    
    if not 'params' in cD:
        cD['params']=[]
    
    res = DB.cmd(cD['cmd'],cD['write'],params=cD['params'],returnDict=1)
    if not cD['write']:
        return json.dumps(res)
    else:
        return {'response':'ok'}

@route('/webbrowser',method='POST')
@headerCheck
def webbrser():
    url = request.json['url']
    if url.startswith('files/'):
        # url = 'http://localhost:'+str(settingD['port'])+'/'+url
        url = chive_url+'/'+url
    elif url == 'mdhelp':
        url = chive_url+'/'+url
        # url = 'http://localhost:'+str(settingD['port'])+'/'+url
    import webbrowser
    webbrowser.open(url)
    return 'ok'

@route('/mdhelp')
def mdhelp():
    return template('docs/markdown_help.html')

#---
#---Main
# Start Bottle

def start_chive(launch=0,use_port=None,dev_mode=False):
    global settings_loaded, chive_url
    settings_loaded = loadSettings()

    if use_port != None:
        settingD['port'] = use_port
    
    # Double Check port not in use
    settingD['port'] = getPort(int(settingD['port'] ))
    
    kargs = {}
    sfx = ''
    if settingD['sslEnabled']=='true' and SERVER == 'gevent': # can't load ssl if wsgiref
        kargs['certfile'] = os.path.join(os.path.expanduser(settingD['sslCertFile']))
        kargs['keyfile'] = os.path.join(os.path.expanduser(settingD['sslKeyFile']))
        sfx = 's'
    
    chive_url = 'http'+sfx+'://localhost:'+str(settingD['port'])
    print(chive_url)
    sys.stdout.flush()

    if launch:
        import webbrowser
        webbrowser.open(chive_url)

    if dev_mode:
        run(host='localhost', port=settingD['port'], debug=True, server=SERVER,**kargs)
    else:
        run(host='localhost', port=settingD['port'], debug=False, quiet=True,server=SERVER,**kargs)

def getCurrentPort():
    return settingD['port']

def getPort(port=None):
    
    if port != None:
        # Return port if available
        if not checkPort(port):
            return port
    
    s = socket.socket()
    s.bind(('localhost', 0))    # Bind to a free port provided by the host.
    port = s.getsockname()[1]  # Return the port number assigned.
    s.close()
    return port
    
def checkPort(port):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    result = sock.connect_ex(('127.0.0.1',port))
    return result == 0

def generateSSL():
    cmd = "openssl req -x509 -newkey rsa:4096 -keyout chive.key -out chive.csr -days 365 -nodes -subj '/CN=localhost'"

if __name__ == '__main__':
    dev_mode = 'dev' in sys.argv
    launch = 'launch' in sys.argv
    start_chive(launch=launch,dev_mode=dev_mode)