const {app, BrowserWindow, dialog, Menu, MenuItem, shell} = require('electron')
const fs = require('fs')
const os = require('os')
const path = require('path')

let win
var python
let WINDOW_LOADED = 0

// Set Current file to path
process.chdir(path.dirname(__dirname))
app.enableSandbox()
// Menu.setApplicationMenu(false)

//Read Config File
function readConfig(path) {
    let settingD = {}
    fs.readFileSync(path, 'utf8').split(/\r?\n/).forEach(line=>{
        var linetrim = line.trim()
        if (!linetrim.startsWith('#') && linetrim.indexOf('=')>-1) {
            let [ky,val] = linetrim.split('=')
            settingD[ky.trim()] = val.trim()
        }
    })
    return settingD
}

//---Settings
// Settings location
const settings_path = path.join(os.homedir(),'.chive')

// Copy default settings if no settings
if (!fs.existsSync(settings_path)) {
    fs.copyFile('default_settings',settings_path,(err)=>{
        if (err) throw err
    })
}

// Get Python Path and other settings
let pythonPath = 'python3'
let settingD = {}
if (fs.existsSync(settings_path)) {
    settingD = readConfig(settings_path)
    if ('pythonPath' in settingD) {
        pythonPath = settingD.pythonPath
    }
}

// Check for SSL
let http_sfx = ''
if (settingD.sslEnabled=='true') {
    http_sfx = 's'
    app.commandLine.appendSwitch('ignore-certificate-errors', 'true')
}

function createWindow () {
    // Create the browser window.
    win = new BrowserWindow({
        width: 1400,
        height: 800,
        icon:'style/chive.png',
        autoHideMenuBar:true,
        webPreferences: {
            nodeIntegration: false,
            devTools: true,
            contextIsolation: true,
            enableRemoteModule: false,
            spellcheck:true,
        }
    })
    
    // Clear Cache (in future check version change before doing this
    win.webContents.session.clearCache(function(){})

    //---f:
    win.loadFile('loading.html').then(()=>{
        // Start Python Bottle Server
        //---o:
        python = require('child_process').spawn(pythonPath, ['./chive.py'])
        
        python.on('error',(err)=>{
            dialog.showMessageBoxSync({
                title:'Error starting Chive',
                message:'Could not start Python. \n\n1. Please install Python if not installed. \n2. Update your pythonPath settings in:\n    '+settings_path,
                buttons:['ok']
            })
        })
        
        python.stdout.on('data',function(data){
            var url = data.toString('utf8')
            if (!WINDOW_LOADED) {
                WINDOW_LOADED=1
                setTimeout(()=>{ // Give Bottle a chance to load
                    // console.log(url)
                    win.loadURL(url+'/tabs')
                }, 500)
            } else {
                console.log(`py: ${data}`)
            }
        })
        python.stderr.on('data', data=> {
            console.error(`stderr: ${data}`)
        })

    })

    // Emitted when the window is closed.
    //---f:
    win.on('closed', () => {
        win = null
    })
    
    //---f:new window
    if (win.webContents.setWindowOpenHandler !== undefined) {
        // Electron 12+
        win.webContents.setWindowOpenHandler(({ url }) => {
            // e.preventDefault()
            shell.openExternal(url)
            return {action: 'deny',}
        })
    } else {
        // Older versions (deprecated eventually)
        win.webContents.on('new-window', function(e, url) {
            e.preventDefault()
            shell.openExternal(url)
        })
    }

    //---Menus
    // Set up Right Click Menu
    //---o:
    const selectionMenu = Menu.buildFromTemplate([
        {role: 'copy'},
        {type: 'separator'},
        {role: 'selectall'},
    ])
    
    //---o:
    const inputMenu = Menu.buildFromTemplate([
        {role: 'cut'},
        {role: 'copy'},
        {role: 'paste'},
        {type: 'separator'},
        {role: 'selectall'},
        {type: 'separator'},
        {role: 'undo'},
        {role: 'redo'},
    ])

    // Spellcheck
    //---o:
    const spellMenu = new Menu()
    inputMenu.insert(0,new MenuItem({
        label:'Spellcheck',
        type:'submenu',
        submenu:spellMenu
    }))
    inputMenu.insert(1,new MenuItem({
        type:'separator',
    }))

    //---f:
    win.webContents.on('context-menu', (e, props) => {
        const { selectionText, isEditable } = props
        
        if (isEditable) {
            // Spellcheck
            spellMenu.clear()
            for (let suggestion of props.dictionarySuggestions) {
                spellMenu.append(new MenuItem({
                    label: suggestion,
                    click: () => win.webContents.replaceMisspelling(suggestion)
                }))
            }
            
            inputMenu.popup(win)
        } else if (selectionText && selectionText.trim() !== '') {
            selectionMenu.popup(win)
        }
    })
    
}

// Create window on ready
//---f:
app.on('ready', createWindow)

// Quit when all windows are closed.
//---f:
app.on('window-all-closed', () => {
    app.quit()
    python.kill('SIGINT')
})

//---f:
app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
    if (win === null) {
        createWindow()
    }
})
