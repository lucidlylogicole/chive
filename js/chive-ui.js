var notebookD = {'':{name:'Uncategorized',sections:[]}}
var sectionD = {}

var CURRENT = {
    content:{},
    notebook:undefined,
    section:undefined,
    double_click_mode:0,
    path:undefined,
    viewall_mode:0,
    view_archived:0,
    notebook_mode:'notebook',
    url_mode:'in',
    NoteEditMode:0,
    TaskView:'default',
    TaskRecentView:'today',
    TaskD:{},
}

//---Window.onload
window.addEventListener('load',function(event) {
    getNotebooks().then(result => {
        loadRecent().then(()=>{
            var urlParams = new URLSearchParams(window.location.search)
            if (location.hash) {
                hashChange()
            } else {
                home()
            }
        })
        
    })
    $P.elm('notebooks').addEventListener('contextmenu', notebookMenu)
    $P.elm('midlist').addEventListener('contextmenu', entryMenu)
    
    
    // Paste Event
    $P.elm('i_md_img_clip').onpaste = editorPaste
    
    $P.elm('i_md_editor').contentWindow.addEventListener('savepress', function (e) {
        mdSave()
    })
    
    window.addEventListener("hashchange", hashChange, false);
    
    $P.listen(document,'keyup',event => {
        if (event.key === 'Escape') {
            $P.menu.close()
        }
    })
    
    //---Drag and drop
    $P.setDropEvent({
        element:'notebooks',
        onDrop:notebookDrop,
        dragOver:notebookDragOver,
        dragLeave:notebookDragLeave,
    })
    
    $P.setDragEvent({
        element:'midlist',
        attr:'data-uuid',
        type:'entry',
        dragStart:function() {
            $P.class.add('notebooks','dropmode')
        },
        dragEnd:function() {
            $P.class.remove('notebooks','dropmode')
        }
    })
    
    // Hide New Tab if not in iframe
    if (window.self === window.top) {
        $P.hide($P.query('.iframe-show'))
    }

    //---Keypress
    document.onkeydown = function(e) {
        if (e.keyCode == 116 && !e.ctrlKey) {
            // Shft-F5
            e.preventDefault();
        } else if ((e.ctrlKey || e.metaKey || e.altKey) && e.keyCode == 87) {
            // Ctrl-W - disable close
            e.preventDefault();
        } else if (e.keyCode == 82 && e.ctrlKey) {
            // Ctrl-R - disable refresh
            e.preventDefault();
        }
    }

})

function home() {
    $P.html('content_viewer',$P.html('tpl_home'))
}

function openUrl(url,trytab) {
    if (trytab && tabCheck()) {
        newTab(url)
    } else if (url.startsWith('#')) {
        document.getElementById(url.slice(1)).scrollIntoView();
    } else {
        if (CURRENT.url_mode == 'in') {
            window.open(url,target="_blank")
        } else {
            openUrlData(url)
        }
    }
}

function hashChange(evt) {
    if (location.hash) {
        
        let hashD = {}
        for (let h of location.hash.slice(1).split('&')) {
            let hspl = h.split('=')
            hashD[hspl[0]] = hspl[1]
        }
        
        if ('note' in hashD) {
            // Load note
            // CURRENT.double_click_mode = 1 // Double click mode to load editor
            loadContent(hashD['note'],{load_notebook:1}).then(()=>{})
        } else if ('notepage' in hashD) {
            loadNoteOnly(hashD['notepage'])
        } else if ('notebook' in hashD) {
            // Notebook
            loadNotebook(hashD['notebook'],1,hashD['section'])
        }

    }
}

function setPageTitle(title) {
    document.title = title
    $P.sendEvent(window, 'titlechanged', title)
}

function newTab(url) {
    // Check if iframe
    $P.sendEvent(window, 'newtab', url)
}

function tabCheck() {
    // Check if inside of a tab / iframe
    return window.self !== window.top
}

function viewSettings() {
    if ($P.elm('setting_details').children.length == 0) {
        viewSettingsData().then(sD=>{
            $P.clear('setting_details')
            $P.create([
                {h:'.flex-row',c:[{h:'.flex-1 align-r',text:'version:'},{h:'.flex-3 pad-l',text:sD.version}]},
                {h:'.flex-row',c:[{h:'.flex-1 align-r',text:'port:'},{h:'.flex-3 pad-l',text:sD.port}]},
                {h:'.flex-row',c:[{h:'.flex-1 align-r',text:'Notebook Path:'},{h:'.flex-3 pad-l',text:sD.notebook}]},
                {h:'.flex-row',c:[{h:'.flex-1 align-r',text:'Autosave (seconds):'},{h:'.flex-3 pad-l',text:sD.autoSave}]},
                ],{parent:'setting_details'})
        })
    }
    $P.toggle('setting_info')
}


//---
//---Notebook
function getNotebooks() {
    var Prom = $P.promise()
    getNotebooksData({'archive':CURRENT.view_archived}).then(result =>{
        sectionD = result.sections
        
        $P.clear('notebooks')
        $P.clear('cb_md_notebook')
        $P.clear('cb_cal_years')
        $P.clear('cb_link_nbk')
        $P.clear('cb_task_nbk')
        
        var childs=[]
        var opt_childs=[{tag:"option",value:'',parent:'cb_md_notebook'}]
        var opt_childs_link=[{tag:"option",value:'',parent:'cb_link_nbk'}]
        var opt_childs_task=[{tag:"option",value:'',parent:'cb_task_nbk'}]
        
        // Sort Notebooks
        var nbk_keys = Object.keys(result.notebooks)
        nbk_keys.sort(function(a, b) {
            var na = result.notebooks[a].name.toLowerCase()
            var nb = result.notebooks[b].name.toLowerCase()
        if (na > nb) {return 1
            } else if (nb > na) {return -1
            } else {return 0}
        })
        
        
        // Add Notebooks to list and comboboxes
        nbk_keys.forEach(function(nuid,ind) {
            n = result.notebooks[nuid]
            
            // Check Archived
            let extra_class = ''
            if (n.archive) {
                extra_class = ' archived'
            }
            
            if (CURRENT.view_archived || !n.archive) {
                
                childs.push({h:'.side-link'+extra_class,'data-id':nuid ,c:[
                    {h:'.flex-row-c', onclick:"loadNotebook('"+nuid+"',true)", c:[
                        {h:'.side-link-img', onclick:'showSections(event)'},
                        {h:'.side-link-name', text:n.name},
                    ]},
                    {h:'.side-link-sections',text:'section'},
                ]})
                    
                opt_childs.push({tag:"option",'value':nuid,text:n.name,parent:'cb_md_notebook'})
                opt_childs_link.push({tag:"option",'value':nuid,text:n.name,parent:'cb_link_nbk'})
                opt_childs_task.push({tag:"option",'value':nuid,text:n.name,parent:'cb_task_nbk'})
            
            }
            notebookD[nuid] = n
        })

        childs.push({h:'.side-link','data-id':'' ,c:[
            {h:'.flex-row-c', onclick:"loadNotebook('',true)", c:[
                {h:'.side-link-img'},
                {h:'.side-link-name', text:'Uncategorized'},
            ]},
            {h:'.side-link-sections',text:'section'},
        ]})

        $P.create({parent:'notebooks',children:childs})
        
        $P.create(opt_childs)
        $P.create(opt_childs_link)
        $P.create(opt_childs_task)
        
        // Update Years
        var yr_childs = []
        for (var yr of result.years) {
            yr_childs.push({tag:'option','value':yr,'text':yr,parent:'cb_cal_years'})
        }
        $P.create(yr_childs)
        
        // Update Path
        CURRENT.path = result.path
        
        Prom.accept()
    })
    return Prom
    
}

function loadNotebook(nbk_id,full,sec_uuid) {
    event.stopPropagation()
    if (nbk_id == 'recent') {
        return loadRecent()
    } else {
        return getContentList({nbk_id:nbk_id,typ:['note','link','file'],parent:null,section:sec_uuid,archive:CURRENT.view_archived}).then(result => {
            $P.clear('midlist')
            if (nbk_id in notebookD) {
                CURRENT.notebook = nbk_id
                CURRENT.section = sec_uuid
                CURRENT.notebook_mode = 'notebook'
                $P.val('cb_md_notebook',nbk_id)
                $P.val('cb_link_nbk',nbk_id)
                $P.val('cb_task_nbk',nbk_id)
                $P.html('midlist_title',notebookD[nbk_id].name)
                
                // Populate Sections
                if (nbk_id) {
                    createNotebookSections(nbk_id)
                }
                
                // Set Page Title
                let title = notebookD[nbk_id].name
                if (sec_uuid) {
                    title += ' - '+sectionD[sec_uuid].name
                }
                // setPageTitle(title)
                
                showEntries(result,{title:title,show_section:sec_uuid})
                if (full) {
                    clearContent()
                    highlightNotebook(nbk_id)
                    
                    // Check if Tasks selected
                    if ($P.class.has('midlist_tasks','active')) {
                        viewTasks()
                    }
                    
                }
                
            }
        })
    }
    
}

function createNotebookSections(nbk_id) {
    // Populate Sections
    let side_elm = $P.query('.side-link[data-id="'+nbk_id+'"] .side-link-sections')[0]
    $P.clear(side_elm)
    sec_elms = []
    for (let sec of notebookD[nbk_id].sections) {
        sec_elms.push({h:'.side-link-section-item',text:sectionD[sec].name, 'data-sec-uuid':sec, onclick:'loadNotebook("'+nbk_id+'",true,"'+sec+'")'})
    }
    $P.create(sec_elms,{parent:side_elm})
}

function notebook_find_keyup(event) {
    txt = $P.val('i_notebook_filter')
    if (txt == '') {
        $P.class.remove($P.query('notebooks','.side-link'),'hidden')
    } else {
        
        for (var nli of $P.query('notebooks','.side-link')) {
            if (nli.innerText.toLowerCase().indexOf(txt)>-1 || txt == '') {
                $P.class.remove(nli,'hidden')
            } else if (!$P.class.has(nli,'hidden')) {
                $P.class.add(nli,'hidden')
            }
        }
        
        if (event.keyCode == 13) {
            $P.query('notebooks','.side-link:not(.hidden)')[0].click()
        }
        
    }
}

function newNotebook() {
    $P.dialog.input ({msg:'Enter the name of the new Notebook'}).then(val => {
        if (val !== undefined) {
            
            var nbk_name = saneFile(val)
            
            // Check if name already exists
            found = false
            for (var ky in notebookD) {
                if (notebookD[ky].name.toLowerCase() == nbk_name.toLowerCase()) {
                    found = true
                }
            }
            if (found) {
                $P.dialog.message({msg:'A notebook with that name already exists'})
            } else {
            
                newNotebookData({name:nbk_name}).then(result =>{
                    getNotebooks().then(()=>{
                        loadNotebook(result.uuid,true)
                    })
                })
            }
        }
    })
}

function highlightNotebook(nbk_uuid) {
    $P.class.remove($P.query('.side-link'),'active')
    $P.class.add($P.query('.side-link[data-id="'+nbk_uuid+'"]'),'active')
}

function notebookMenu(event) {
    event.preventDefault();
    let elm = event.target
    if ($P.class.has(elm,'side-link-section-item')) {
        // Section Menu
        CURRENT.section_menu = $P.attr(elm,'data-sec-uuid')
        CURRENT.notebook_menu = sectionD[CURRENT.section_menu].nbk_uuid
        $P.menu.show('notebook_section_menu')
    } else {
        // Notebook Menu
        if (!($P.class.has(elm,'side-link'))) {
            elm = elm.closest('.side-link')
        }
        
        if ($P.class.has(elm,'side-link')) {
            let nbk_uuid = $P.attr(elm,'data-id')
            if (nbk_uuid != '') {
                CURRENT.notebook_menu = nbk_uuid
                
                // Setup Archive Menu Item
                if (notebookD[nbk_uuid].archive) {
                    $P.html('menu_notebook_archive','Unarchive')
                } else {
                    $P.html('menu_notebook_archive','Archive')
                }
                
                $P.menu.show('notebook_menu')
            }
        }
    }
}

function renameNotebook(nbk_uuid) {
    if (nbk_uuid === undefined) {
        nbk_uuid = CURRENT.notebook_menu
    }
    if (nbk_uuid in notebookD) {
        $P.dialog.input({msg:'Rename Notebook',val:notebookD[nbk_uuid].name}).then(result=>{
            if (result !== undefined && result !== notebookD[nbk_uuid].name) {
                let new_name = saneFile(result)
                renameNotebookData({uuid:nbk_uuid,name:new_name}).then(result => {
                    if (result.response == 'ok') {
                        getNotebooks().then(()=>{
                            if (CURRENT.notebook == nbk_uuid) {
                                loadNotebook(nbk_uuid,1)
                            } else {
                                // rename in midlist
                                for (var elm of $P.query(".nbk_name[data-nbk-uuid='"+nbk_uuid+"']")) {
                                    elm.innerText=new_name
                                }
                            }
                        })
                    } else {
                        $P.dialog.message({msg:result.response,btns:['ok'],title:'Error Renaming Notebook'})
                    }
                })
            }
        })
    }
}

function deleteNotebook(nbk_uuid) {
    if (nbk_uuid === undefined) {
        nbk_uuid = CURRENT.notebook_menu
    }
    if (nbk_uuid in notebookD) {
        var nbk_name = notebookD[nbk_uuid].name
        $P.dialog.message({msg:'Do you want to delete the notebook: <b>'+nbk_name+'</b><br>and all associated notes and files?',btns:['yes','no'],title:'Delete Notebook'}).then(result=>{
            if (result == 'yes') {
                $P.dialog.message({msg:'Are you really sure??????',btns:['yes','no'],title:'Delete Notebook'}).then(result=>{
                    if (result == 'yes') {
                        deleteNotebookData({uuid:nbk_uuid}).then(result => {
                            if (result.response == 'ok') {
                                getNotebooks()
                                clearContent()
                                $P.clear('midlist')
                                $P.dialog.message({msg:'The notebook has been deleted except for the files/attachments.<br><br>To delete the attachments please go to: '+CURRENT.path+'/'+nbk_name})
                            } else {
                                showError(result)
                            }
                        })
                    }
                })
            }
        })
    }
}

function newNotebookTab(nbk_uuid,sec_uuid) {
    if (nbk_uuid === undefined) {
        nbk_uuid = CURRENT.notebook
    }
    if (nbk_uuid === undefined) {
        newTab('/')
    } else {
        let sec = ''
        if (sec_uuid) {
            sec = '&section='+sec_uuid
        }
        newTab('/#notebook='+nbk_uuid+sec)
    }
}

function archiveNotebook(uuid) {
    if (uuid === undefined) {
        uuid = CURRENT.notebook_menu
    }
    if (uuid in notebookD) {
        var nbk_name = notebookD[uuid].name
        
        let arc = !notebookD[uuid].archive
        let ok = 1
        
        if (ok) {
            archiveNotebookData({uuid:uuid,archive:arc}).then(result=>{
                if (result.response != 'ok') {
                    alert(result.status)
                } else {
                    
                    notebookD[uuid].archive = arc
                    
                    let elm = $P.query('.side-link[data-id="'+uuid+'"]')[0]
                    if (arc) {
                        $P.class.add(elm,'archived')
                    } else {
                        $P.class.remove(elm,'archived')
                    }
                }
            })
        }
        
    }
}

//---Sections
function showSections(ev) {
    ev.stopPropagation()
    let nbk_id = $P.attr(ev.target.closest('.side-link'),'data-id')
    
    let side_elm = $P.query('.side-link[data-id="'+nbk_id+'"] .side-link-sections')[0]
    let side_img = $P.query('.side-link[data-id="'+nbk_id+'"] .side-link-img')[0]
    
    // Toggle If Visible
    if ($P.class.has(side_elm,'active')) {
        $P.class.remove(side_elm,'active')
        $P.class.remove(side_img,'open')
    } else {
        
        $P.class.add(side_elm,'active')
        $P.class.add(side_img,'open')
        createNotebookSections(nbk_id)
    }

}
function addSection(nbk_uuid) {
    if (nbk_uuid === undefined) {
        nbk_uuid = CURRENT.notebook_menu
    }

    $P.dialog.input({msg:'Enter the name of the new Section'}).then(sname => {
        if (sname !== undefined) {
            newSectionData({nbk_uuid:nbk_uuid,name:sname}).then(result=>{
                // Add to Section list
                sectionD[result.uuid] = {name:sname,nbk_uuid:nbk_uuid}
                notebookD[nbk_uuid]['sections'].push(result.uuid)
                
                createNotebookSections(nbk_uuid)
                
                updateTaskSections(nbk_uuid)
            })
        }
    })
}

function renameSection(uuid) {
    if (uuid === undefined) {
        uuid = CURRENT.section_menu
    }
    
    $P.dialog.input({msg:'Edit the Section name',val:sectionD[uuid].name}).then(sname => {
        if (sname !== undefined) {
            
            renameSectionData({name:sname,uuid:uuid}).then(()=>{
                // Rename in Section list
                sectionD[uuid].name = sname
                
                // Rename in task editor drop down
                for (var elm of $P.query("option[value='"+uuid+"']")) {
                    elm.innerText=sname
                }
                // rename in content viewer tasks
                for (var elm of $P.query(".task-header[data-sec-uuid='"+uuid+"']")) {
                    elm.innerText=sname
                }
                
                // rename in midlist
                for (var elm of $P.query(".nbk_name[data-sec-uuid='"+uuid+"']")) {
                    elm.innerText=sname
                }
                
                for (var elm of $P.query(".side-link-section-item[data-sec-uuid='"+uuid+"']")) {
                    elm.innerText=sname
                }
                
            })
        }
    })
}

function deleteSection(sec_uuid) {
    if (sec_uuid === undefined) {
        sec_uuid = CURRENT.section_menu
    }
    msg = 'Do you want to remove the section: <b>'+sectionD[sec_uuid].name+'</b>?<br>(All content will be moved to undefined)'
    $P.dialog.message({msg:msg, btns:['yes','no'], title:'Remove Section'}).then(val => {
        if (val == 'yes') {
            deleteSectionData(sec_uuid).then(result => {
                // Remove From Stuff
                let nbk_uuid = sectionD[sec_uuid].nbk_uuid
                $P.array.remove(notebookD[nbk_uuid].sections,sec_uuid)
                delete sectionD[sec_uuid]
                
                $P.remove($P.query('.sec-item[data-id="'+sec_uuid+'"]'))
                $P.remove($P.query(".nbk_name[data-sec-uuid='"+sec_uuid+"']"))
                
                createNotebookSections(nbk_uuid)
                
                // Update Task Section
                updateTaskSections(nbk_uuid)

            })
        }
    })
}

//---Drag and Drop Moving
function notebookDragOver(ev) {
    // Section Hover
    if ($P.class.has(ev.target,'side-link-section-item')) {
        $P.class.add(ev.target,'dragover')
    }
    // Notebook Hover
    let elm = ev.target.closest('.side-link')
    if (elm) {
        $P.class.add(ev.target.closest('.side-link'),'dragover')
    }
    
}
function notebookDragLeave(ev) {
    $P.class.remove(ev.target,'dragover')
    // Notebook
    let elm = ev.target.closest('.side-link')
    if (elm) {
        $P.class.remove(elm,'dragover')
    }
}

function notebookDrop(ev, c_uuid, drag_type) {
    if (c_uuid) {
        let elm = ev.target.closest('.side-link')
        // Move entry to notebook
        if (elm) {
            let nbk_uuid = $P.attr(elm,'data-id')
            let sec_uuid
            // Get Section
            if ($P.class.has(ev.target,'side-link-section-item')) {
                sec_uuid = $P.attr(ev.target,'data-sec-uuid')
            }
            
            moveContentData({uuid:c_uuid,nbk_uuid:nbk_uuid,sec_uuid:sec_uuid}).then(result => {
                reloadMidlist(true)
            })
        }
        
    }
    $P.class.remove($P.query('.dragover'),'dragover')
}

//---
//---Midlist
function showEntries(entries,options) {
    var childs = []
    if (options.title !== undefined) {
        $P.html('midlist_title',options.title)
        setPageTitle(options.title)
    }
    
    // Add Section if Selected
    if (options.show_section) {
        childs.push({tag:'h3',class:'pebble margin-n align-c entry-section',text:sectionD[options.show_section].name})
    }
    
    // Add Notes
    for (var n of entries) {
        var date_txt = ''
        if (options.show_notebook && n.nbk_uuid !='') {
            date_txt += '<span class="nbk_name" data-nbk-uuid="'+n.nbk_uuid+'">'+notebookD[n.nbk_uuid].name+'</span><br>'
        } else if (n.section) {
            // Sections
            date_txt += '<span class="nbk_name" data-sec-uuid="'+n.section+'">'+sectionD[n.section].name+'</span><br>'
        }
        try {
            date_txt += '<span class="entry_date">'+n.create_time.slice(0,10)+'</span>'
        } catch (e) {
            
        }
        // Pin Icon Setup
        var pin_class = 'pin'
        if (n.pin) {pin_class='pinned'}
        
        // Archive Setup
        let archive_class = ''
        if (n.archive) {archive_class=' archived'}
        
        childs.push({'class':'entry'+archive_class,
            onclick:"loadContent('"+n.uuid+"')",
            ondblclick:"CURRENT.double_click_mode=1",
            'data-uuid':n.uuid,
            'draggable':true,
            children:[
                {tag:'img',class:pin_class},
                {tag:'img','src':'style/img/'+n.typ+'.svg'},
                {tag:'span',text:n.title,class:'flex'},
                {tag:'span','class':'mid_info',text:date_txt},
            ]
        })

    }
    
    $P.create({parent:'midlist',children:childs})
}

function linkDblClick(cid) {
    CURRENT.double_click_mode=1
}


function midlist_find_keyup(event) {
    txt = $P.val('i_midlist_filter')
    if (txt == '') {
        $P.class.remove($P.query('midlist','.entry'),'hidden')
    } else {
        
        for (var nli of $P.query('midlist','.entry')) {
            if (nli.innerText.toLowerCase().indexOf(txt)>-1 || txt == '') {
                $P.class.remove(nli,'hidden')
            } else if (!$P.class.has(nli,'hidden')) {
                $P.class.add(nli,'hidden')
            }
        }
    }
}

function updateEntry(cid,data) {
    // Update Title
    var eelm = $P.query('.entry[data-uuid="'+cid+'"] .flex')
    if (eelm.length > 0 && data.title !== undefined) {
        eelm[0].innerHTML = data.title
    }
    // Update Notebook Name
    if (data.nbk_uuid !== undefined) {
        var nelm = $P.query('.entry[data-uuid="'+cid+'"] .nbk_name')
        if (nelm.length > 0) {
            nelm[0].innerHTML = notebookD[data.nbk_uuid].name
        }
    }
}

function deleteEntry(cid) {
    var eelm = $P.query('.entry[data-uuid="'+cid+'"]')
    if (eelm.length > 0) {
        $P.remove(eelm[0])
    }
}

async function viewAllEntries() {
    $P.clear('content_viewer')
    options = {
        clear:false,
        viewall:true,
    }
    setPageTitle($P.query('#midlist_title')[0].innerText)
    CURRENT.viewall_mode=1
    $P.create({parent:'content_viewer',tag:'h1',class:'pebble align-c',text:$P.query('#midlist_title')[0].innerText})
    for (var elm of $P.query('midlist','.entry')) {
        var uuid = $P.attr(elm,'data-uuid')
        if (uuid != null) {
            await loadContent(uuid,options)
        }
    }

    $P.class.remove($P.query('.entry'),'active')
}

async function reloadMidlist(full,cid) {
    if (CURRENT.notebook_mode == 'recent') {
        await loadRecent(full)
    } else {
        await loadNotebook(CURRENT.notebook,full)
    }
    if (cid !== undefined) {
        // Highlight Current
        $P.class.remove($P.query('.entry'),'active')
        $P.class.add($P.query('.entry[data-uuid="'+cid+'"]'),'active')
    }
}

function highlightMidItem(cid) {
    $P.class.remove($P.query('.entry'),'active')
    $P.class.add($P.query('.entry[data-uuid="'+cid+'"]'),'active')
}

function midlistKeypress(event) {
    let ok = 0
    let cur_elm = $P.query('.entry.active')
    if (cur_elm.length > 0) {
        let next_elm
        if (event.keyCode == 38) {
        // Up
            next_elm = cur_elm[0].previousSibling
        } else if (event.keyCode == 40) {
        // Down
            next_elm = cur_elm[0].nextSibling
        }
        if (next_elm) {
            next_elm.click()
            let rectElem = next_elm.getBoundingClientRect(), rectContainer=event.target.getBoundingClientRect();
            if (rectElem.bottom > rectContainer.bottom) next_elm.scrollIntoView(false);
            if (rectElem.top < rectContainer.top) next_elm.scrollIntoView();
            
            next_elm.scrollIntoView(false)
            ok = 1
        }
    }
    if (ok) {
        event.preventDefault()
    }
}

//---Entry Menu
function entryMenu(event) {
    event.preventDefault()
    let evt = event
    var elm
    if ($P.class.has(event.target,'entry')) {
        elm = event.target
    } else if ($P.class.has(event.target.parentNode,'entry')) {
        elm = event.target.parentNode
    }
    
    if (elm) {
        CURRENT.midlist = $P.attr(elm,'data-uuid')
        $P.menu.show('entry_menu')
        
        // Set Menu pinned
        var pin = 1
        if ($P.class.has(elm.children[0],'pinned')){
            $P.class.set('img_pin','pin')
            $P.html('entry_menu_pin_text','Unpin')
        } else {
            $P.class.set('img_pin','pinned')
            $P.html('entry_menu_pin_text','Pin')
        }
        
        // Set Archived
        if ($P.class.has(elm,'archived')) {
            $P.html('menu_content_archive','Unarchive')
        } else {
            $P.html('menu_content_archive','Archive')
        }
            
    }
}

function currentEntryEdit() {
    loadContent(CURRENT.midlist).then(()=>{editContent()})
}

function currentEntryDelete() {
    loadContent(CURRENT.midlist).then(()=>{deleteContent()})   
}

function currentEntryPin() {
    var mid_elms = $P.query('#midlist [data-uuid="'+CURRENT.midlist+'"]')
    if (mid_elms.length > 0) {
        var pin = 1
        if ($P.class.has(mid_elms[0].children[0],'pinned')){pin = 0}
        var cmd = "UPDATE content SET pin=? WHERE uuid=?"
        queryData(cmd,[pin,CURRENT.midlist],1).then(()=>{
            if (pin) {
                $P.class.set('img_pin','pinned')
                $P.class.set($P.query('.entry[data-uuid="'+CURRENT.midlist+'"] img')[0],'pinned')
            } else {
                $P.class.set('img_pin','pin')
                $P.class.set($P.query('.entry[data-uuid="'+CURRENT.midlist+'"] img')[0],'pin')
            }
        })
    }
}

function openCurrentEntry() {
    loadContent(CURRENT.midlist).then(()=>{
        openExternal()
        // Load again to fix hanging issue
        setTimeout(()=>{loadContent(CURRENT.midlist)},1000)
    })
}
function openExternal() {
    if (CURRENT.content.typ == 'link') {
        openUrl(JSON.parse(CURRENT.content.content).url)
    } else if (CURRENT.content.typ == 'file') {
        openFile(CURRENT.content.uuid)
    } else if (CURRENT.content.typ == 'note') {
        openUrl(location.origin+'#notepage='+CURRENT.content.uuid)
    }
}

function currentEntryExport(export_type) {
    exportContent(CURRENT.midlist,export_type)
}

function currentEntryNewTab() {
    newTab('/#note='+CURRENT.midlist)
}

function currentEntryArchive() {
    
    let elm = $P.query('.entry[data-uuid="'+CURRENT.midlist+'"]')[0]
    
    let arc = 1
    // Set Archived
    if ($P.class.has(elm,'archived')) {
        arc = 0
    }
    
    archiveContentData({uuid:CURRENT.midlist,archive:arc}).then(result=>{
        if (result.response != 'ok') {
            alert(result.status)
        } else {
            if (arc) {
                $P.class.add(elm,'archived')
            } else {
                $P.class.remove(elm,'archived')
            }
        }
    })

}

function viewFolder(nbk_uuid) {
    if (!nbk_uuid){
        alert('no notebook is selected')
        return
    }
    
    data_post('opennotebookfolder',{uuid:nbk_uuid},'Opening Folder').then(result => {
        if (result.status != 'ok') {
            alert(result.status)
        }
    })
}

function appendContent(uuid) {
    loadContent(uuid,{clear:false,viewall:true})
}


//---
//---Content
function loadContent(cid,options) {

    return getContentData(cid).then(result => {
        if (options === undefined) {
            options = {}
        }
        if ((options.clear === undefined || options.clear) && !options.update) {
            $P.clear('content_viewer')
        }
        
        var childs = [
            {tag:'h1',class:'content-title flex-row', style:'align-items: flex-start;',children:[{tag:'span',class:'flex',text:result.title}]},
        ]
        
        childs[0].children.push(
            {tag:'button',class:'btn-img float-r btn-content-edit',children:[{tag:'img',src:'/style/img/edit.svg',onclick:"mdEditor('"+cid+"')"}]})
        
        if (result.typ == 'link' || result.typ == 'file') {
            childs[0].children[1].children[0].onclick="editLink('"+cid+"')"
        }
        if (options.viewall) {
            childs[0].class += ' viewall'
        }
        CURRENT.viewall_mode=options.viewall
        
        if (result.typ == 'note') {
            // Note Fields
            childs.push({tag:'div',class:'content-note',text:mdCompile(result.content)})
        } else if (result.typ == 'link') {
            // Link Fields
            var lD = JSON.parse(result.content)
            var dsc = lD.desc
            if (dsc !== null) {
                dsc = (dsc)
            }
            childs.push({text:mdCompile(dsc)})
            childs.push({class:'spacer'})
            childs.push({tag:'a',onclick:"openUrl('"+lD.url+"')",title:lD.url,text:lD.url})
            
        } else if (result.typ == 'file') {
            
            if (result.content != '') {
                var lD = JSON.parse(result.content)
                var dsc = lD.desc
                if (dsc !== null) {
                    dsc = mdCompile(dsc)
                }
                childs.push({text:dsc})
                childs.push({class:'spacer'})
            }
            
            childs.push({tag:'a',onclick:"openFile('"+cid+"')",text:result.title})
        }
        
        // Add Footer
        childs.push({class:'content-footer',children:[
            {tag:'span',class:'content-date',text:'Created on '+result.create_time,children:[
                {tag:'img',title:'edit create date',src:'style/img/edit.svg',onclick:"editCreateDate('"+cid+"')"},
            ]},
            {tag:'span',class:'content-info',text:'i',onclick:"copyIDClipboard('"+cid+"')",title:'ID: '+cid+' (click to copy to clipboard)'},
            {tag:'input',class:'hidden',value:cid,id:'info_'+cid},
        ]})
        
        // Create or Update Content
        if (options.update){
            var content_elm = $P.query('[content-id="'+cid+'"]')[0]
            if (content_elm !== undefined) {
                $P.clear(content_elm)
                var elms = $P.create({parent:content_elm,
                    text:' ',
                    children:childs})
            }
        
        } else {
            
            $P.create({parent:'content_viewer',
                'class':'pebble content_block shadow',
                'content-id':cid,
                children:childs})
        }
        
        // Set all links to open in new tab
        overrideContentLinks()
        
        // Set Current Reult
        CURRENT.content=result
        CURRENT.content['uuid']=cid
        
        // If Double click
        if (CURRENT.double_click_mode) {
            if (result.typ == 'link') {
                openUrl(JSON.parse(result.content).url)
            } else if (result.typ == 'note') {
                mdEditor(cid)
            } else if (result.typ == 'file') {
                openFile(cid)
            }
            CURRENT.double_click_mode = 0
        }
        
        
        // Highlightmidlist
        for (var elm of $P.query('pre code')) {
            hljs.highlightBlock(elm)
        }
        
        // Show Edit Toolbar
        if (!options.viewall && !options.update) {
            $P.show($P.query('.menu-current-content'))
            
            // Pinned
            var cls = 'pin'
            if (result.pin) {cls='pinned'}
            
            // Highlight Current
            highlightMidItem(cid)
            
            setPageTitle(result.title)
        }
        $P.elm('content_viewer').scrollTop = 0;

        // Load Notebook
        if (options.load_notebook) {
            loadNotebook(result.nbk_uuid).then(()=>{
                // Highlight Notebook
                highlightNotebook(result.nbk_uuid)
                // Highlight Current
                highlightMidItem(cid)
                $P.query('.entry[data-uuid="'+cid+'"]')[0].scrollIntoView(false)
                setPageTitle(result.title)
            })
        }


    })
}

function overrideContentLinks() {
    // Set all links to open in new tab
    var anchors = $P.query('content_viewer','a')
    for (var i=0; i<anchors.length; i++){
        var anc = anchors[i]
        if ($P.attr(anc,'href') == null){
        } else if ($P.attr(anc,'href').startsWith('attach/')) {
            $P.attr(anc,'onclick',"openFile('"+$P.attr(anc,'href').split('/')[1]+"')")
            anc.removeAttribute('href')
        } else if ($P.attr(anc,'href').startsWith('link/')) {
            $P.attr(anc,'onclick',"loadContent('"+$P.attr(anc,'href').split('/')[1]+"')")
            anc.removeAttribute('href')
        } else {
            $P.attr(anc,'onclick',"openUrl('"+$P.attr(anc,'href')+"')")
            $P.attr(anc,'title',$P.attr(anc,'href'))
            anc.removeAttribute('href')
        }
    }
}

function editContent(cid) {
    if (cid === undefined) {cid = CURRENT.content.uuid}
    if (cid !== undefined) {
        if (CURRENT.content.typ == 'note') {
            mdEditor(cid)
        } else if (CURRENT.content.typ == 'link' || CURRENT.content.typ=='file') {
            editLink(cid)
        } 
    }
}

function deleteContent(cid) {
    if (cid === undefined) {cid = CURRENT.content.uuid}
    if (cid !== undefined) {
        msg = 'Do you want to delete the '+CURRENT.content.typ
        msg += ':<br><b>'+CURRENT.content.title+'<b>'
        $P.dialog.message ({msg:msg, btns:['yes','no'], title:'Delete Content'}).then(val => {
            if (val == 'yes') {
                deleteContentData(cid).then(result => {
                    deleteEntry(cid)
                    if (!CURRENT.viewall_mode) {
                        if (CURRENT.NoteEditMode) {
                            mdClose(1)
                        }
                        clearContent()
                        setPageTitle('Chive')
                    }
                })
            }
        })
    }
}

function clearContent() {
    CURRENT.content = undefined
    $P.clear('content_viewer')
    $P.hide($P.query('.menu-current-content'))
}

function title_search_keydown(event) {
    if (event.keyCode == 13) {
        searchContent($P.val('titlebar_search'))
    }
}

function editCreateDate(cid) {
    $P.dialog.input({msg:'Enter Create Date (yyyy-mm-dd)'}).then(v=>{
        var cmd = "UPDATE content SET create_time=? WHERE uuid=?"
        queryData(cmd,[v,cid],1).then(()=>{
            $P.query('[content-id="'+cid+'"] .content-date')[0].innerText='Created on '+v
            var delm = $P.query('[data-uuid="'+cid+'"] .entry_date')
            if (delm.length > 0) {
                delm[0].innerText=v
            }
        })
    })
}

function copyIDClipboard(uid) {
    $P.show('info_'+uid)
    document.getElementById('info_'+uid).select()
    document.execCommand("copy")
    $P.hide('info_'+uid)
}

//---Content Queries
function searchContent(txt) {
    if (txt != '') {
        getContentList({
            search_text:txt,
            archive:CURRENT.view_archived,
            content:$P.val('titlebar_search_content')}).then(result => {
                CURRENT.notebook = undefined
                CURRENT.section = undefined
                CURRENT.notebook_mode = 'search'
                $P.clear('midlist')
                showEntries(result,{title:'Search: '+txt,show_notebook:1})
                $P.class.remove($P.query('.side-link'),'active')
                if ($P.class.has('midlist_tasks','active')) {
                    viewTasks()
                }
        })
    }
}

function loadRecent(full) {
    return getContentList({'recent':30,parent:null,archive:CURRENT.view_archived}).then(result => {
        CURRENT.notebook = undefined
        CURRENT.section = undefined
        $P.clear('midlist')
        if (full==1) {
            clearContent()
        }
        $P.class.remove($P.query('.side-link'),'active')
        CURRENT.notebook_mode = 'recent'
        showEntries(result,{title:'Recent', show_notebook:1})

        $P.class.add($P.query('.side-link-recent'),'active')

        // Check if Tasks selected
        if ($P.class.has('midlist_tasks','active')) {
            if (CURRENT.TaskRecentView == 'notebook') {
                viewNotebookTasks()
            } else {
                viewTasks(undefined,CURRENT.TaskRecentView)
            }
        }
    })
}

function loadMonth(month) {
    getContentList({'month':month,year:$P.val('cb_cal_years'),archive:1}).then(result => {
        CURRENT.notebook = undefined
        CURRENT.section = undefined
        $P.clear('midlist')
        clearContent()
        $P.class.remove($P.query('.side-link'),'active')
        CURRENT.notebook_mode = 'month'
        CURRENT.calendar = [parseInt($P.val('cb_cal_years')),month]
        showEntries(result,{title:$P.date.months[month-1]+' '+$P.val('cb_cal_years'),show_notebook:1})
        
        if ($P.class.has('midlist_tasks','active')) {
            viewTasks()
        }
        
    })
}

function loadNoteOnly(uuid) {
    $P.clear(document.body)
    getContentData(uuid).then(result => {
        elm = {class:'center-container shadow',style:"margin-top:20px;margin-bottom:20px;",children:[
            {class:'content bg-w pebble', children:[
                {tag:'h1',class:'content-title flex-row',children:[{tag:'span',class:'flex',text:result.title}]},
                {tag:'div',class:'content-note',text:mdCompile(result.content)},
            ]}
        ]}
            
    
        $P.create(elm,{parent:document.body})

        // Highlightmidlist
        for (var elm of $P.query('pre code')) {
            hljs.highlightBlock(elm)
        }

    })
}

function openNoteLink(elm) {
    loadContent($P.attr(elm,'data-uuid'))
}

//---Find
function findContent() {
    var txt = $P.val('i_content_find')
    if (event.key === 'Enter') {
        $P.menu.close()
        window.find(txt,0,0,1,0,0)
        document.getSelection().anchorNode.parentElement.scrollIntoView()
    }
}

//---
//---Markdown Editor

function mdEditor(cid) {
    $P.hide('side_nav')
    editorMode(true)
    $P.show('editor')
    $P.attr('editor','data-cid',cid)
    getContentData(cid).then(result => {
        $P.elm('i_md_editor').contentWindow.setText(result.content)
        $P.elm('i_md_title').value = result.title
        $P.attr('i_md_title','initial-title',result.title)
        $P.elm('cb_md_notebook').value = result.nbk_uuid

        mdNotebookChange().then(()=>{
            $P.elm('cb_md_section').value = result.section
        })
        
        $P.elm('i_md_editor').contentWindow.start_autosave()
        $P.elm('i_md_editor').contentWindow.editor.focus()
    })
}

function mdSave() {
    var cid = $P.attr('editor','data-cid')
    var cD = {
        uuid:cid,
        content:$P.elm('i_md_editor').contentWindow.getText(),
        title:$P.elm('i_md_title').value,
        nbk_uuid:$P.elm('cb_md_notebook').value,
        typ:'note',
        section:$P.elm('cb_md_section').value,
    }
    setPageTitle($P.elm('i_md_title').value)
    if (cid == 'new') {
        return newContentData(cD).then(result => {
            $P.attr('editor','data-cid',result.uuid)
            reloadMidlist(0).then(()=>{
                highlightMidItem(result.uuid)
                setPageTitle($P.elm('i_md_title').value)
            })
            loadContent(result.uuid)
        })
        
    } else {
        return saveContentData(cD).then(result => {
            // Update content without loading
            var content_elm = $P.query('[content-id="'+cid+'"] .content-note')[0]
            if (content_elm !== undefined) {
                $P.clear(content_elm)
                $P.html(content_elm,mdCompile(cD.content))
                var title_elm = $P.query('[content-id="'+cid+'"] h1.content-title span')[0]
                if (title_elm !== undefined) {
                    title_elm.innerText = cD.title
                }
                $P.html(content_elm,mdCompile(cD.content))
                overrideContentLinks()
            } else {
                // Just in case run loadContent
                loadContent(cid,{update:1,viewall:CURRENT.viewall_mode})
            }
        })
    }
    
}

function mdInsert(txt) {
    $P.elm('i_md_editor').contentWindow.editor.insert(txt)
}

function mdClose(cancel) {
    var cid = $P.attr('editor','data-cid')

    if (!cancel) {
        var title_change = $P.elm('i_md_title').value != $P.attr('i_md_title','initial-title') || CURRENT.content.nbk_uuid != $P.val('cb_md_notebook')
        if (CURRENT.content !== undefined) {
            var section_change = CURRENT.content.section != $P.val('cb_md_section')
        }
    }
    if (!cancel && ($P.elm('i_md_editor').contentWindow.EDITOR_CHANGED || title_change || section_change)) {
        mdSave().then(cD=>{
            updateEntry(cid,{title:$P.elm('i_md_title').value,nbk_uuid:$P.val('cb_md_notebook')})
            
            if (section_change) {
                loadNotebook($P.val('cb_md_notebook'),true).then(()=>{
                    loadContent(cid)
                })
            }
            
        })
    }
    $P.hide('editor')
    $P.show('side_nav')
    editorMode(false)
    $P.elm('i_md_editor').contentWindow.clear_autosave()

    // Highlight Code Block
    for (var elm of $P.query('pre code')) {
        hljs.highlightBlock(elm)
    }
}

function newNote() {
    $P.attr('editor','data-cid','new')
    $P.elm('i_md_editor').contentWindow.setText('')
    $P.elm('i_md_title').value = ''
    $P.hide('side_nav')
    $P.show('editor')
    $P.clear('content_viewer')
    var nwdt = new Date()
    editorMode(1)
    $P.elm('i_md_title').focus()
    $P.elm('i_md_editor').contentWindow.start_autosave()
    mdNotebookChange()
    setPageTitle('new note')
    mdSave()
}

function editorMode(mode) {
    if (mode) {
        CURRENT.NoteEditMode=1
        $P.hide('bg_new')
        $P.hide('titlebar_search')
        $P.hide('titlebar_search_content')
        $P.show('bg_edit')
    } else {
        CURRENT.NoteEditMode=0
        $P.show('bg_new')
        $P.show('titlebar_search')
        $P.show('titlebar_search_content')
        $P.hide('bg_edit')
    }
}

async function editorPaste (event){
    items = (event.clipboardData || event.originalEvent.clipboardData).items;
    for (var i=0; i<items.length; i++)
    if (items[i].kind === 'file') {
        
        var blob = items[i].getAsFile();

        // Save file if new
        if ($P.attr('editor','data-cid') == 'new') {
            await mdSave()
        }

        // Build FOrm Data to Send
        var fd = new FormData()
        fd.append("filedata", blob)
        fd.append("nbk_uuid", $P.val('cb_md_notebook'))
        fd.append("parent", $P.attr('editor','data-cid'))
        fd.append("ext",blob.name.split('.').slice(-1))
        fd.append("typ",'attach')
        
        // Upload File
        uploadFileData(fd).then(result =>{
            // Insert text into text editor
            var editor = $P.elm('i_md_editor')
            var img = '![](file/'+result.uuid+')'
            mdInsert(img)
            $P.hide('image_insert')
            mdSave()
        })
    }
}

function mdNotebookChange(edit) {
    var Prom = $P.promise()
    $P.clear('cb_md_section')
    var opt_childs = [{tag:"option",value:'',parent:'cb_md_section'}]
    for (var s of notebookD[$P.val('cb_md_notebook')]['sections']) {
        opt_childs.push({tag:"option",'value':s,text:sectionD[s].name,parent:'cb_md_section'})
        if (s == CURRENT.section) {
            opt_childs.slice(-1)[0].selected='selected'
        }
    }
    $P.create(opt_childs)
    if (edit) {
        mdEditorChangeFlag()
    }
    Prom.accept()
    
    return Prom
}

function mdEditorChangeFlag() {
    $P.elm('i_md_editor').contentWindow.EDITOR_CHANGED=1
}

//---Markdown parsing
function mdCompile(txt) {
    
    // Convert Markdown
    result = cmarkdRender(txt)
    result = $P.replaceAll(result,'<img ','<img onclick="$P.dialog.image({element:event.target})" ')
    result = $P.replaceAll(result,'<a href="note/','<a onclick="openNoteLink(this)" data-uuid="')

    return result
}


//---
//---Files
function newFile() {
    if (CURRENT.notebook === undefined) {
        $P.dialog.message({msg:'Please select a notebook first to attach a file'})
    } else {
        document.getElementById('file_input').click()
    }
}

function fileSelected(event) {
    var files = event.target.files
    
    var fd = new FormData()
    fd.append("filedata", files[0])
    fd.append("nbk_uuid", CURRENT.notebook)
    fd.append("filename", files[0].name)
    uploadFileData(fd).then(result=>{
        if (result.status) {
            showError(result.status,20)
        }
        $P.val('file_input','')
        loadNotebook(CURRENT.notebook)
    })
}

function openFile(cid) {
    openFileData({uuid:cid})
}

function newBlankFile() {
    if (CURRENT.notebook === undefined) {
        $P.dialog.message({msg:'Please select a notebook first to create a new file'})
    } else {
        $P.dialog.input({msg:'Enter the name of the file and the extension'}).then(sname => {
            if (sname !== undefined) {
                var fd = new FormData()
                fd.append("filedata", '')
                fd.append("nbk_uuid", CURRENT.notebook)
                fd.append("filename", sname)
                uploadFileData(fd).then(result=>{
                    if (result.status) {
                        showError(result.status,20)
                    }
                    loadNotebook(CURRENT.notebook)
                })
            }
        })
    }
}

function newNoteFile(mode) {
    document.getElementById('file_note_input').click()
    CURRENT.attach_mode=mode
    $P.hide('image_insert')
}

function noteFileSelected(event) {
    var files = event.target.files
    
    var fd = new FormData()
    fd.append("filedata", files[0])
    // fd.append("nbk_uuid", CURRENT.notebook)
    fd.append("nbk_uuid", $P.val('cb_md_notebook'))
    fd.append("filename", files[0].name)
    fd.append('typ','attach')
    fd.append('parent',$P.attr('editor','data-cid'))
    uploadFileData(fd).then(result=>{
        if (result.status) {
            showError(result.status,20)
        } else {
            txt = ''
            var url_pfx = 'attach'
            if (CURRENT.attach_mode == 'img') {
                txt += '!'
                url_pfx = 'file'
            }
            txt += '['+result.filename+']('+url_pfx+'/'+result.uuid+')'
            mdInsert(txt)
            mdSave().then(()=>{
                $P.val('file_note_input','')
            })
        }
    })
}


//---
//--- Links
function newLink() {
    $P.show('link_editor')
    $P.attr('link_editor','data-id','new')
    $P.val('i_link_title','')
    $P.val('i_link_url','')
    $P.val('te_link_desc','')
    $P.show('link_editor_url_row')
    linkNotebookChange().then(()=>{
        if (CURRENT.section !== undefined) {
            $P.elm('cb_link_section').value = CURRENT.section
        }
    })
    
}

function editLink(cid) {
    getContentData(cid).then(result => {
        $P.show('link_editor')
        $P.attr('link_editor','data-id',cid)
        $P.val('cb_link_nbk',result.nbk_uuid)
        linkNotebookChange().then(()=>{
            $P.elm('cb_link_section').value = result.section
        })
        $P.val('i_link_title',result.title)
        if (result.content != '') {
            var cD = JSON.parse(result.content)
            $P.val('i_link_url',cD.url)
            $P.val('te_link_desc',cD.desc)
        } else {
            $P.val('te_link_desc','')
        }
        if (result.typ == 'link') {
            $P.show('link_editor_url_row')
        } else {
            $P.hide('link_editor_url_row')
        }
    })
}

function saveLink() {
    var linkD = $P.val('link_editor')
    linkD['uuid'] = $P.attr('link_editor','data-id')
    linkD['nbk_uuid'] = $P.val('cb_link_nbk')
    linkD['section'] = $P.val('cb_link_section')
    linkD['content'] = JSON.stringify({'desc':linkD['desc'],'url':linkD.url})
    
    if (linkD['uuid'] == 'new') {
        linkD['typ'] = 'link'
        newContentData(linkD).then(async result => {
            $P.hide('link_editor')
            await reloadMidlist(result['uuid'])
            loadContent(result['uuid'])
        })
    } else {
        linkD.typ = CURRENT.content.typ
        saveContentData(linkD).then(result => {
            $P.hide('link_editor')
            loadContent(linkD['uuid'],{update:1,viewall:CURRENT.viewall_mode})
            if (CURRENT.notebook != linkD.nbk_uuid && CURRENT.notebook != undefined) {
                deleteEntry(linkD['uuid'])
            } else {
                updateEntry(linkD.uuid,{title:linkD.title,nbk_uuid:linkD.nbk_uuid})
            }
        })
    }
}

function linkNotebookChange() {
    var Prom = $P.promise()
    $P.clear('cb_link_section')
    var opt_childs = [{tag:"option",value:'',parent:'cb_link_section'}]
    for (var s of notebookD[$P.val('cb_link_nbk')]['sections']) {
        opt_childs.push({tag:"option",'value':s,text:sectionD[s].name,parent:'cb_link_section'})
    }
    $P.create(opt_childs)
    Prom.accept()
    
    return Prom
}

//---
//---Tasks
function viewTasks(task_id,view_mode,all_status) {
    var queryD
    if (task_id !== undefined){
        queryD.task_id = task_id
        queryD.mode='task'
        queryD.view='default'
    } else {
        queryD = {
            mode:CURRENT.notebook_mode,
            nbk_uuid:CURRENT.notebook,
            view:'default',
            all_status:all_status,
        }
        
        if (view_mode !== undefined) {
            queryD.view = view_mode
            CURRENT.TaskRecentView = queryD.view
        } else if (queryD.mode == 'recent') {
            // Default to a week for recent
            if (CURRENT.TaskView !='default') {
                queryD.view = CURRENT.TaskView
            }else {
                queryD.view = 'today'
            }
            
        } else if (queryD.mode == 'search') {
            queryD.search = $P.val('titlebar_search')
            queryD.all_cols = true
        } else if (queryD.mode == 'month') {
            queryD.all_cols = true
            queryD.date = CURRENT.calendar
        }
        
    }
    CURRENT.TaskView = queryD.view
    getTaskData(queryD).then(result => {
        
        // Clear content
        $P.class.remove($P.query('.entry'),'active')
        $P.class.add('midlist_tasks','active')
        clearContent()
        
        var task_elms
        
        if (CURRENT.notebook_mode == 'recent' || CURRENT.notebook_mode == 'search' || CURRENT.notebook_mode == 'month') {
        //---Recent Tasks
            // Task Header
            var task_ttl = "Today's"
            if (queryD.view == 'week'){task_ttl = "This Week's"
            } else if (queryD.view == 'active'){task_ttl = "Active"}
            task_elms = [
                getRecentTaskTitle(task_ttl+' Tasks')
            ]
            setPageTitle(task_ttl+' Tasks')
            // Adjust title for search tasks
            if (CURRENT.notebook_mode == 'search') {
                task_elms = [
                    {tag:'h1',class:'flex-row flex-center',children:[
                        {tag:'img',src:'style/img/task.svg'},
                        {tag2:'h2',text:'&nbsp;Tasks <small>(search)</small>',class:'flex'},
                    ],}
                ]
            } else if (CURRENT.notebook_mode == 'month') {
                // Adjust title for Calendar view
                task_elms = [
                    {tag:'h1',class:'flex-row flex-center',children:[
                        {tag:'img',src:'style/img/task.svg'},
                        {tag2:'h2',text:'&nbsp;'+$P.date.months[CURRENT.calendar[1]-1]+' '+CURRENT.calendar[0],class:'flex'},
                    ],}
                ]
            }
            
            // organize by notebook
            var ntD = {}
            result.tasks.forEach((val,ind)=>{
                if (!(val.nbk_uuid in ntD)) {
                    ntD[val.nbk_uuid] = {'':[]}
                }
                if (!val.section) {
                    ntD[val.nbk_uuid][''].push(createTask(val))
                } else {
                    if (!(val.section in ntD[val.nbk_uuid])) {
                        ntD[val.nbk_uuid][val.section]=[]
                    }
                    ntD[val.nbk_uuid][val.section].push(createTask(val))
                }
            })
            
            // Load Notebooks and Tasks
            for (var nky in ntD) {
                task_elms.push({tag:'h2',class:'task-header',text:notebookD[nky].name, onclick:"loadNotebook('"+nky+"',true)", children:[{tag:'img', src:'style/img/notebook.svg'}]})
                ntD[nky][''].forEach((ntask,ind)=>{
                    task_elms.push(ntask)
                }) 
                for (var s of notebookD[nky].sections) {
                    if (s in ntD[nky]) {
                        task_elms.push({tag:'h3',class:'task-header',text:sectionD[s].name,'data-sec-uuid':s})
                        ntD[nky][s].forEach((ntask,ind)=>{
                            task_elms.push(ntask)
                        }) 
                    }
                }
            }
            
            
        } else {
            
        //---Notebook Tasks
            task_elms = [{tag:'h1',class:'flex-row flex-center',
                children:[
                    {tag:'img',src:'style/img/task.svg','class':'float-l'},
                    {class:'flex',text:'&nbsp;'+notebookD[CURRENT.notebook].name+' Tasks'}
                ]
            }]
            
            setPageTitle(notebookD[CURRENT.notebook].name+' Tasks')
            
            // Sections
            var sec_tasks = {}
            result.tasks.forEach((val,ind)=>{
                if (val.section) {
                    if (!(val.section in sec_tasks)) {
                        sec_tasks[val.section]=[]
                    }
                    sec_tasks[val.section].push(val)
                } else {
                    task_elms.push(createTask(val))
                }
            })
            
            // Add Sections
            for (var s of notebookD[CURRENT.notebook].sections) {
                if (s in sec_tasks) {
                    task_elms.push({tag:'h3',class:'task-header',text:sectionD[s].name,'data-sec-uuid':s})
                    for (var t of sec_tasks[s]) {
                        task_elms.push(createTask(t))
                    }
                }
            }
            
            // Add complete toggle button
            if (task_elms.length > 1) {
                task_elms.push({class:'spacer'})
                task_elms.push({tag:'button',class:'btn',text:'view complete',onclick:"viewTasks(undefined,undefined,true)"})
            }
        }
        
        // Create Elements
        $P.create({parent:'content_viewer',children:task_elms})
        $P.class.add($P.query('.task-title-'+queryD.view),'active')
        
    })
}

function getRecentTaskTitle(title) {
    return {tag:'h1',class:'flex-row flex-center',children:[
        {tag:'img',src:'style/img/task.svg'},
        {tag2:'h2',text:'&nbsp;'+title,class:'flex'},
        {class:'task-title-link task-title-notebook', title:'Notebook Task Overview', onclick:"viewNotebookTasks()",children:[{tag:'img','src':'style/img/notebook.svg'}]},
        {class:'task-title-link task-title-today', title:'Today Tasks', onclick:"viewTasks(undefined,'today')",children:[{tag:'img','src':'style/img/today.svg'}]},
        {class:'task-title-link task-title-week', title:'Week Tasks', onclick:"viewTasks(undefined,'week')",children:[{tag:'img','src':'style/img/week.svg'}]},
        {class:'task-title-link task-title-active', title:'Active Tasks', onclick:"viewTasks(undefined,'active')",children:[{tag:'img','src':'style/img/active_black.svg'}]},
    ],}
}

function taskMenu(event) {
    var par = event.target.parentElement
    if (event.target.tagName == 'IMG') {
        par = event.target.parentElement.parentElement
    }
    CURRENT.TASK_MENU_ID = $P.attr(par,'data-id')
    $P.menu.show('task_menu')
}

function createTask(taskD) {
    // Creates Task Element (Dictionary for $P.create)
    var pcls = ''
    var tchilds = [
        {class:'task-icon',onclick:"taskMenu(event)",'children':[{tag:'img','src':'style/img/'+taskD.status+'.svg'}]},
        {class:'task-title',text:taskD.title,onclick:'editCurrentTask(this)'},
    ]

    // Target Date
    if (taskD.target_date) {
        tchilds.push({class:'task-item-due-date',text:taskD.target_date,onclick:'editCurrentTask(this)'})
    }

    // Add Priority
    if (taskD.priority != 'normal') {
        tchilds.push({class:'task-icon',children:[{tag:'img','src':'style/img/'+taskD.priority+'.png'}]})
    }
    if (taskD.priority == 'bug') {
        pcls = 'bug'
    } else if (taskD.priority == 'important') {
        pcls = 'important'
    }
    
    // Check if Done
    if (taskD.status == 'done' || taskD.status == 'cancel') {
        pcls = 'done'
    }
    
    // Link to Note
    
    if (taskD.content_uuid) {
        tchilds.push({class:'task-icon',title:'view note',onclick:"viewTaskContent('"+taskD.content_uuid+"')",children:[{tag:'img','src':'style/img/note.svg'}]})
    }
    
    var task_elm = {
        class:'task-item rad-s '+pcls,
        'data-id':taskD.uuid,
        'data-display':'flex',
        'data-plan-date':taskD['plan_date'],
        children:tchilds,
    }
    
    return task_elm
}

function editCurrentTask(self) {
    var uuid = $P.attr(self.parentElement,'data-id')
    editTask(uuid)
}

function editTask(task_id) {
    getTaskData({uuid:task_id,mode:'task'}).then(result => {
        var tD = result.tasks[0]
        CURRENT.TaskD = tD
        var nl = ''
        if (tD.content_uuid) {
            nl = tD.content_uuid
        }
        $P.attr('task_editor','data-id',task_id)
        $P.val('cb_task_nbk',tD.nbk_uuid)
        $P.val('task-edit-title',tD.title)
        $P.val('task-edit-status',tD.status)
        $P.val('task-edit-priority',tD.priority)
        $P.val('task-edit-plan-date',tD.plan_date)
        $P.val('task-edit-due-date',tD.target_date)
        $P.val('task-edit-notes',JSON.parse(tD.content).notes)
        $P.val('task-edit-note-link',nl)
        
        var h = '<b>Created:</b> '+tD.create_time.slice(0,16)
        if (tD.complete_time) {
            h += '<br><b>Complete:</b> '+tD.complete_time.slice(0,16)
        }
        $P.html('l_task_dates',h)
        
        $P.show('task_editor')
        taskNotebookChange().then(() => {
            $P.val('cb_task_section',tD.section)
        })
    })
}

function newTask() {
    $P.show('task_editor')
    $P.attr('task_editor','data-id','new')
    $P.val('task-edit-title','')
    $P.val('task-edit-status','active')
    $P.val('task-edit-priority','normal')
    $P.val('task-edit-plan-date','')
    $P.val('task-edit-due-date','')
    $P.val('task-edit-notes','')
    $P.val('task-edit-note-link','')
    
    if (CURRENT.NoteEditMode == 1) {
        $P.val('task-edit-note-link',CURRENT.content.uuid)
        $P.val('cb_task_nbk',CURRENT.content.nbk_uuid)
    }
    
    taskNotebookChange()
}

function saveTask() {
    $P.elm('b_task_save').disabled = 'disabled'
    
    var taskD = $P.val('task_editor')
    taskD['uuid'] = $P.attr('task_editor','data-id')
    
    // if new then get id and set in title
    // disable button while saving
    saveTaskData(taskD).then(result => {
        if (taskD['uuid'] == 'new') {
            $P.attr('task_editor','data-id',result['uuid'])
        }
        $P.elm('b_task_save').disabled = ''
        
        // Update entry
        if (taskD['uuid'] == 'new') {
            if (!CURRENT.NoteEditMode) {
                viewTasks()
            }
        } else {
            
            var task_elm = $P.create(createTask({
                uuid:result.uuid,
                title:taskD['task-edit-title'],
                priority:taskD['task-edit-priority'],
                status:taskD['task-edit-status'],
                target_date:taskD['task-edit-due-date'],
                plan_date:taskD['task-edit-plan-date'],
                content_uuid:taskD['task-edit-note-link'],
            }))
            
            // Reload If section or notebook changed
            if (CURRENT.TaskD.section != taskD.sec_uuid || CURRENT.TaskD.nbk_uuid != taskD.nbk_uuid || CURRENT.TaskD.plan_date != taskD.plan_date) {
                viewTasks()
            } else {
                var cur_elm = $P.query('.task-item[data-id="'+result.uuid+'"]')[0]
                cur_elm.parentNode.replaceChild(task_elm,cur_elm)
            }
            
            
            
        }
    })
    
}

function closeTaskEditor() {
    $P.hide('task_editor')
}

function deleteTask(self) {
    var uuid = $P.attr('task_editor','data-id')
    if (uuid == 'new') {
        closeTaskEditor()
    } else {
        // Delete message
        msg = 'Do you want to delete this task?'
        $P.dialog.message ({msg:msg, btns:['yes','no'], title:'Delete Task'}).then(val => {
            if (val == 'yes') {
                deleteTaskData(uuid).then(result => {
                    $P.remove($P.query('.task-item[data-id="'+uuid+'"]')[0])
                    closeTaskEditor()
                })
            }
        })
    }
}

function planSnooze(days) {

    let plan_date
    if ($P.val('task-edit-plan-date') != '') {
        plan_date = new Date($P.val('task-edit-plan-date')+' 00:00')
    } else {
        plan_date = new Date()
    }
    let tdy = new Date()
    if (plan_date < tdy) {
        plan_date = tdy
    }
    
    plan_date.setDate(plan_date.getDate() + days)
    $P.val('task-edit-plan-date',$P.date.str(plan_date))

}

function planToday() {
    var plan_date = new Date()
    $P.val('task-edit-plan-date',$P.date.str(plan_date))
}

function changeTask(mode,val) {
    $P.menu.close()
    if (mode == 'edit') {
        editTask(CURRENT.TASK_MENU_ID)
    } else if (mode == 'status') {
        updateTaskData({uuid:CURRENT.TASK_MENU_ID,col:mode,val:val})
        $P.query('.task-item[data-id="'+CURRENT.TASK_MENU_ID+'"] .task-icon img')[0].src='style/img/'+val+'.svg'
    }
}

function taskNotebookChange() {
    var Prom = $P.promise()
    $P.clear('cb_task_section')
    var opt_childs = [{tag:"option",value:'',parent:'cb_task_section'}]
    for (var s of notebookD[$P.val('cb_task_nbk')]['sections']) {
        opt_childs.push({tag:"option",'value':s,text:sectionD[s].name,parent:'cb_task_section'})
        if (s == CURRENT.section) {
            opt_childs.slice(-1)[0].selected='selected'
        }
    }
    $P.create(opt_childs)
    Prom.accept()
    
    return Prom
}

function viewNotebookTasks() {
    clearContent()
    CURRENT.TaskRecentView = 'notebook'
    getNotebookTaskData().then(result=>{
        
        var task_elms = [
            getRecentTaskTitle('Notebook Tasks')
        ]
        
        result.tasks.forEach((val,ind)=>{
            task_elms.push({class:'task-item rad-s task-notebook-item', onclick:"loadNotebook('"+val.uuid+"',true)",children:[
                {tag:'img',src:'style/img/notebook.svg',style:"height:1.6rem;"},
                {text:val.name, class:'flex task-notebook-item-title'},
                {class:'flex-row',children:[
                    {tag:'img',src:'style/img/active.svg'},
                    {text:val.cnt_active,style:"color:rgb(29,129,51);"},
                ]},
                {class:'space-sm'},
                {class:'flex-row',children:[
                    {tag:'img',src:'style/img/backlog.svg'},
                    {text:val.cnt_backlog,style:"color:rgb(80,80,80);"},
                ]},
            ]})
        })
        
        $P.create({parent:'content_viewer',children:task_elms})
        $P.class.add($P.query('.task-title-notebook'),'active')
    })
}

function viewTaskContent(uuid) {
    // Load a note(content) from a task
    if (tabCheck()) {
        newTab('/#note='+uuid)
    } else {
        loadContent(uuid)
        
    }
}

function updateTaskSections(nbk_uuid) {
    // Update Sections in Task Editor
    if (nbk_uuid == $P.val('cb_task_nbk')) {
        var cur_sec = $P.val('cb_task_section')
        taskNotebookChange().then(()=>{
            $P.val('cb_task_section',cur_sec)
        })
    }
}

//---
//---Archived
function toggleArchived() {
    CURRENT.view_archived = !CURRENT.view_archived
    reloadMidlist()
    getNotebooks()
    if (CURRENT.view_archived) {
        $P.html('midlist_menu_view_archived','Hide Archived Content')
    } else {
        $P.html('midlist_menu_view_archived','View Archived Content')
    }
}

//---
//---Exports
async function exportContent(cid,export_type) {
    
    // Export Type
    let filetype = 'text/html'
    if (export_type === undefined) {export_type = 'md'}
    if (export_type == 'md') {filetype = 'text/markdown'}
    
    // Get Content
    getContentData(cid).then(async result => {
        
        let content = '# ' + result.title+'\n\n----\n\n'
        let dl_elm = document.getElementById('download_element');
        
        // Content
        if (result.typ == 'link') {
            // Link
            let lD = JSON.parse(result.content)
            content += lD.desc
            content += '\n\n['+lD.url+']('+lD.url+')'
        } else if (result.typ == 'note') {
            // Note
            content += result.content
            
        } else if (result.typ == 'file') {
            // File
            dl_elm.href = '/file/'+cid
            dl_elm.setAttribute("download", result.title)
            dl_elm.click()
            return
        }
        
        // Parse Markdown to HTML
        if (export_type == 'html') {
            // Get Images and base64 them
            img_res = await queryData("SELECT uuid,title FROM content WHERE parent=? and typ='attach'",[cid])
            for (let imgD of img_res) {
                content =  $P.replaceAll(content,'(file/'+imgD.uuid+')','[file/'+imgD.uuid+']')
                let img_blob = await $P.ajax({url:'/file/'+imgD.uuid,responseType:'blob'}) //.then(async img_blob => {
                let file_res = await $P.file.read('readAsDataURL',img_blob)
                content += '\n\n[file/'+imgD.uuid+']:'+file_res
            }
            content = mdCompile(content)
            content = `<style>\n    body {font-family:ubuntu,calibri,helvetica;font-size:1.1rem;font-weight:100;color:rgb(60,60,60);margin:0;}
    h1,h2,h3,h4 {color:rgb(40,40,40);font-weight:200;}
    button, input[type=button],input[type=submit],input[type=reset],select {background:rgb(230,230,230);background: linear-gradient(to bottom, rgb(240,240,240) 0%,rgb(230,230,23)100%);border:1px solid rgb(210,210,210);padding:6px 12px;box-sizing:border-box;border-radius:4px;font-size:1.1rem;line-height:1.1rem;cursor:pointer;margin:2px 0p;color:rgb(40,40,40);}
    button:hover,input[type=button]:hover,input[type=submit]:hover,input[type=reset]:hover {background:rgb(220,220,220);border:1px solid rgb(200,200,200);}
    select {padding:4px 8px;color:rgb(40,40,40);}
    input:not([type=checkbox]):not([type=radio]):not([type=button]), textarea {margin:2px 0px;padding:4px 8px;box-sizing:border-box;font-size:1.1rem;border:1px solid rg(210,210,210);border-radius:4px;}
    a, a:visited {color:rgb(50,117,190);text-decoration:none;}
    a:hover {color:rgb(29,79,134);}
    b {font-weight:500;}
    hr {border:0px;border-top: 1px solid rgb(180,180,180);}
    pre, code  {background:rgb(240,240,240);border-radius:4px;border:1px solid rgb(230,230,230);padding:8px;box-sizing:border-box;line-height:1.1rem;color:rgb(80,80,80);}
    pre {overflow-x:auto;}
    code {padding:0px 4px;}
    pre code {border:0;padding:0px;background:transparent;}
    img{max-width:100%;}
    .paper {background:white;box-sizing:border-box;padding:16px;max-width:960px;margin:16px auto;}
    </style>\n<div class="paper">`+ content+'</div>'
        }
        
        // Create data blob and add to href
        let data = new Blob([content])
        dl_elm.href = URL.createObjectURL(data)
        dl_elm.setAttribute("download", result.title+'.'+export_type);
        dl_elm.click()
    })
    
}


//---
//---Other
function saneFile(txt) {
    return txt.replace(/[^a-z0-9'_\- ]/gi, ' ')
}

function showError(txt,time,noerror) {
    $P.show('error_message')
    if (!noerror) {
        txt = 'ERROR: '+txt
    }
    $P.html('error_text', txt)
}
